#! /usr/bin/env python3

from modelBuilder import *
from functools import partial
from hyperopt import fmin, tpe, hp, rand
from hyperopt.mongoexp import MongoTrials
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adadelta
from keras import backend as K
from tensorflow import set_random_seed
import numpy as np
from hyperopt import STATUS_OK
import keras
nb_eval = 2
batch_size = 128
epochs = 1
seeds = range(10)
activation = 'relu'
output = 'softmax'
loss = 'categorical_crossentropy'
data_set = 'mnist'

def reset_weights(model, verbose):
    session = K.get_session()
    for layer in model.layers:
        for v in layer.__dict__:
            v_arg = getattr(layer, v)
            if hasattr(v_arg, 'initializer'):
                initializer_method = getattr(v_arg, 'initializer')
                initializer_method.run(session=session)
                if verbose: print('reinitializing layer {}.{}'.format(layer.name, v))

def run_sampling(space):
    seeds = range(10)
    e = space['e']
    g = space['g']
    model = Sequential()
    verbose = 0
    activation = 'sigmoid'
    output = 'softmax'
    loss = 'categorical_crossentropy'
    model.add(Dense(512, activation=activation, input_shape=(784,)))
    model.add(Dense(512, activation=activation))
    model.add(Dense(10, activation=output))
    model.compile(loss=loss,
                  optimizer=Adadelta(rho=g, epsilon=e),
                  metrics=['accuracy'])

    for j, seed in enumerate(seeds):
        if verbose: print('seed No-{0}'.format(j + 1))
        acc_entries = np.empty(shape=len(seeds))
        loss_entries = np.empty(shape=len(seeds))
        np.random.seed(seed)
        set_random_seed(seed)
        reset_weights(model, verbose)
        model.fit(x_train, y_train,
                       batch_size=batch_size,
                       epochs=epochs,
                       verbose=verbose,
                       validation_data=(x_test, y_test))
        score = model.evaluate(x_test, y_test, verbose=verbose)
        loss_entries[j] = score[0]
        acc_entries[j] = score[1]
        mean_loss = np.mean(loss_entries)
        mean_acc = np.mean(acc_entries)
        var_loss = np.var(loss_entries, ddof=1)
        var_acc = np.var(acc_entries, ddof=1)
    return {'loss_entries': loss_entries,
        'loss': mean_loss,
        'var_loss': var_loss,
        'acc_entries' : acc_entries,
        'acc': mean_acc,
        'var_acc': var_acc,
        'status': STATUS_OK}




epsis = [1e-16, 1e-10, 1e-9, 1e-8, 1e-7,
         1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1]
epsi_test = [1e-24, 1e-5]
file = 'hp_relu'
count = 1713


epsi = [1, 1e-1, 1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8,
        1e-9, 1e-10, 1e-11, 1e-12, 1e-13, 1e-14, 1e-15, 1e-16]
space = {
    'e': hp.choice('e', epsi),
    'g': hp.quniform('g', 0, 1, 0.01)
}

while True:
    build = ModelBuilder(count, data_set, activation, output, loss, file, seeds)
    run = partial(build.run, seeds, batch_size, epochs, verbose=1)
    best = fmin(
        fn=run,
        space=space,
        algo=rand.suggest,
        max_evals=50
    )
    print(best)
    del build
    build = None
    count += 50

