import numpy as np
from scipy.optimize import curve_fit





def choose_model(x_data, y_data):
    try:
        popt_const, _ = curve_fit(constant_model, x_data, y_data)
        const_score = sum([(constant_model(x, *popt_const) - y)**2
                      for x, y in zip(x_data, y_data)])
    except RuntimeError:
        const_score = float('inf')
    try:
        popt_linear, _ = curve_fit(linear_model, x_data, y_data)
        linear_score = sum([(linear_model(x, *popt_linear) - y)**2
                        for x, y in zip(x_data, y_data)])
    except RuntimeError:
        linear_score = float('inf')
    try:
        popt_exp, _ = curve_fit(exp_model, x_data, y_data)
        exp_score = sum([(exp_model(x, *popt_exp) - y)**2
                         for x, y in zip(x_data, y_data)])
    except RuntimeError:
        exp_score = float('inf')

    scores = np.argmin([const_score, linear_score, exp_score])
    print(scores)
    return 'const' if scores == 0 else 'linear' if scores == 1 else 'exp'
np.random.seed(1729)
f_const = (lambda x: 5)
f_linear = (lambda x: 3*x + 2)
f_exp = (lambda x: 7*np.exp(-12*x) + 15)
xdata = np.linspace(0, 10, 50)
ydata_noise =  np.random.normal(size=xdata.size)
ydata_const = [f_const(x) for x in xdata]
ydata_linear = [f_linear(x) for x in xdata] + ydata_noise
ydata_exp = [f_exp(x) for x in xdata] + ydata_noise
print(ydata_exp)
print(choose_model(xdata, ydata_const))
print(choose_model(xdata, ydata_linear))
print(choose_model(xdata, ydata_exp))
