#! /usr/bin/env python3
from NNChooser import *
from NNModelsBuilder import *
from NeuralNetwork import NeuralNetwork
from NNFunctions import *
from WeightInitializer import *
from WeightRegression import *
from NNUtilities import *
from NNGD import *
import math
parse_dataset('spambase.data', 'min_max', 57, [[0, 1], [1, 0]], True, header=None)
data, target = load_parsed_data('spambase.data')
new_gen = np.random.RandomState(42)
epoch = 400
linear_rate = 0.01
ld = 0.1
mini_batch = 1
layers = [len(data[0]), 100, len(target[0])]
adadelta = ADADELTA_adadecay(layers, 1e-4, NoReg())
sgd = SGD(linear_rate, NoReg())
data_set = [[x, y] for x, y in zip(data, target)]
new_gen.shuffle(data_set)
cut = math.ceil(len(data_set)*0.8)
train = data_set[:cut]
valid = data_set[cut:]
"""
nn1 = NeuralNetwork(layers, act_func=sigmoid,
                   out_func=sigmoid, error_func=L2,
                   w_init=normalized_variance, gd_method=sgd, dropout_prob=1, rand_gen=new_gen)
nn1.fit(train, epoch, 10, valid, True, True, True, True, True)
"""
epoch = 1
epsis = [16, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
epsi_test = [24, 5]
epsi = BasicChooser(epsis, len(epsis))
gammas = [0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99]

gamma = BasicChooser(gammas, len(gammas))
gamma_close_one = ReversePowChooser(15, 0, 16)
builder = Adadelta_builder(epsi, gamma_close_one, 42, 16, layers)
builder.build(epoch, train, valid, descriptor=['{:.2e}', '1{:.0e}'])
df = builder.get_df()
plot_df(df, epoch)

