#! /usr/bin/env python3
from NeuralNetwork import *
from NNFunctions import *
from NNUtilities import *
from NNModelsBuilder import *

from NNGD import *
from NNChooser import *
layers = [2, 2]
epoch = 10
mini_batch = 128
epsis = [1e-24, 1e-16, 1e-10, 1e-9, 1e-8, 1e-7,
         1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1]
epsi_test = [1e-24, 1e-5]
epsi = BasicChooser(epsis, len(epsis))
gammas = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]

build = Ada_builder([1e-5], [0.9], dataset='and')
build.run(range(1), epoch, mini_batch,  layers,
          NoReg(), sigmoid, softmax_f, cross_ent, uniform_sig_initializer)
build.output_csv('sig_c-e.csv')

