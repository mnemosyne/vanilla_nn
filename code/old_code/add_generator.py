import pandas as pd
import numpy as np

data = []
for i in range(5000):
    a = float(np.random.randint(2))
    b = float(np.random.randint(2))
    if a == 1. and b == 1.:
        c = 1.
    else:
        c = 0.
    data.append([a, b, c])
df = pd.DataFrame(data, columns=['a', 'b', 'c'])
df.to_csv('and.csv')