import numpy as np

from scipy.optimize import curve_fit
from scipy.optimize import brent
def solver_minimize_f(c, data, count):
    return abs(
        (((data[3] * pow(data[4] / c, count) + data[2]) * c + data[1])
         * c + data[0]) / (1 - c))

class CurveFit:
    def __init__(self):
        self.G = 13
        self.gammas = np.array([0.10, 0.20, 0.30, 0.40, 0.53, 0.64, 0.73, 0.81, 0.87, 0.92, 0.95, 0.98, 0.99],
                               dtype=float)
        self.lgammas = np.empty(self.gammas.shape)
        self.tgammas = np.empty(self.gammas.shape)
        self.c0 = 0.
        self.P = np.empty(shape=(3, self.G), dtype=float)
        self.bias = np.empty(shape=(3, self.G), dtype=float)
        self.gain = np.empty(shape=(3, self.G), dtype=float)
        self.decay = np.empty(shape=(3, self.G), dtype=float)
        self.error = np.empty(shape=(3, self.G), dtype=float)
        self.count = self.igamma = self.imode = 0
        self.values = []
        self.clear()
        print("end init curve fitting")

    def get_decay(self):
        return self.decay[self.imode, self.igamma]

    def get_bias(self):
        return self.bias[self.imode, self.igamma]

    def get_gain(self):
        return self.gain[self.imode, self.igamma]

    def get_error(self):
        return self.error[self.imode, self.igamma]

    def get_gamma(self):
        return self.gammas[self.igamma]

    def get_mode(self):
        return 'e' if self.imode == 2 else 'a' if self.imode == 1 else 'c'

    def clear(self):
        self.lgammas = np.log(self.gammas) / np.log(self.gammas[self.G - 1]) * (1 - self.gammas)
        self.tgammas = np.ones_like(self.tgammas)
        self.P = np.zeros_like(self.P)
        self.bias = np.zeros_like(self.bias)
        self.error = np.zeros_like(self.error)
        self.decay = np.zeros_like(self.decay)
        self.gain = np.zeros_like(self.gain)
        self.count = self.igamma = self.imode = 0
        self.values = []

    def add(self, c):
        m00 = (1 - self.tgammas)
        self.P = self.gammas * (self.P - self.c0) + self.c0

        if self.count > 0:
            self.bias[0, :] = self.P[0, :] + m00
            self.error[0, :] = self.lgammas * (c - self.bias[0, :])**2 + self.gammas * self.error[0, :]
            if self.count > 1:
                m01 = (1 - self.tgammas / self.gammas)
                m10 = self.tgammas * self.count - (1 - self.tgammas) / (1 - self.gammas)
                m11 = (self.tgammas * self.count + (self.gammas * (self.gammas - 2) + self.tgammas) /
                       (1 - self.gammas)) / self.gammas
                D = m00 * m11 - m01 * m10
                for g, d in enumerate(D):
                    if d != 0:
                        self.gain[1, g] = (self.P[1, g] * m00[g] - self.P[0, g] * m01[g]) / d
                        self.bias[1, g] = (self.P[0, g] * m11[g] - self.P[1, g] * m10[g]) / d
                        self.error[1, g] = self.lgammas[g] * (c - self.bias[1, g])**2 + \
                                           self.gammas[g] * self.error[1, g]
                if (self.count > 2):
                    gp3p2 = self.gammas * self.P[2, :] - self.P[1, :]
                    gp2p1 = self.gammas * self.P[1, :] - self.P[0, :]
                    g2 = self.gammas**2
                    data = [self.gammas * gp2p1 * self.tgammas + g2 * (self.P[0, :] - self.P[1, :]),
                            (self.P[0, :] - g2 * self.P[2, :]) * self.tgammas - g2 * (self.P[0, :] - self.P[2, :]),
                            gp3p2 * self.tgammas + g2 * (self.P[1, :] - self.P[2, :]),
                            (1 - self.gammas) * (gp2p1 - gp3p2),
                            self.gammas
                            ]
                    cdecays = [brent(solver_minimize_f,
                                     args=([data[0][i], data[1][i], data[2][i], data[3][i], data[4][i]], self.count),
                                     brack=(0, 10), tol=1e-12)
                               for i in range(len(self.gammas))]
                    for i, g in enumerate(self.gammas):
                        if cdecays[i] < g:
                            cT = pow(g / cdecays[i], self.count)
                            m10 = (1 - g) * (1 - cT) / (cdecays[i] - g)
                            m11 = (1 - g) * (1 / cdecays[i] - cT / g) / (cdecays[i] - g)
                            d = m00[i] * m11 - m01[i] * m10
                        if d != 0:
                            self.decay[2, i] = -1 / np.log(cdecays[i])
                            self.gain[2, i] = (self.P[1, i] * m00[i] - self.P[0, i] * m01[i]) / d
                            self.bias[2, i] = (self.P[0, i] * m11 - self.P[1, i] * m10) / d
                            self.error[2, i] = self.lgammas[i] * (c - (self.bias[2, i] + self.gain[2, i])) \
                                               + self.gammas[i] * self.error[2, i]
        self.tgammas *= self.gammas
        emin = float('inf')
        self.igamma = self.imode = 0
        for g in range(self.G-1, -1, -1):
            for m in range(3):
                if((m < self.count) and ((m < 2 ) or (0 < self.error[m, g])) and (self.error[m][g] < emin)):
                    self.igamma = g
                    self.imode = m
                    emin = self.error[m, g]
            self.c0 = c
            self.values.append(self.c0)
            self.count += 1


    def to_string(self):
        return " 'count' : {0}, 'decay' : {1}, 'gain' : {2}, 'bias' : {3}," \
               " 'error' : {4}, 'gamma' : '{5}', 'mode' : {6}".format(
            self.count, self.get_decay(), self.get_gain(), self.get_bias(),
            self.get_error(), self.get_gamma(), self.get_mode())




class SimpleFitting:
    def __init__(self, gamma, window):
        self.const = (lambda x, a: a + np.random.normal(size=1))
        self.linear = (lambda x, a, b: a*x + b)
        self.exp = (lambda x, a, b, c: a*np.exp(-b * x) + c)
        self.obs = []
        self.gamma = gamma
        self.window = window
        self.model = None

    def add_obs(self, new_obs):
        smooth_obs = new_obs if len(self.obs) == 0 else \
            self.gamma*self.obs[len(self.obs) - 1] \
            + (1 - self.gamma)*new_obs

        self.obs.append(smooth_obs)
        if len(self.obs) > 2:
            self.choose_model()

    def get_model(self):
        return self.model

    def choose_model(self):
        window = min(self.window, len(self.obs))
        try:
            popt_const, _ = curve_fit(self.const, range(window), self.obs[-window:])
            const_score = sum([(self.const(x, *popt_const) - y)**2
                          for x, y in zip(range(window), self.obs[-window:])])
        except RuntimeError:
            const_score = float('inf')
        try:
            popt_linear, _ = curve_fit(self.linear, range(window), self.obs[-window:])
            linear_score = sum([(self.linear(x, *popt_linear) - y)**2
                            for x, y in zip(range(window), self.obs[-window:])])
        except RuntimeError:
            linear_score = float('inf')
        try:
            popt_exp, _ = curve_fit(self.exp, range(window), self.obs[-window:])
            exp_score = sum([(self.exp(x, *popt_exp) - y)**2
                             for x, y in zip(range(window), self.obs[-window:])])
        except RuntimeError:
            exp_score = float('inf')
    
        scores = np.argmin([const_score, linear_score, exp_score])
        self.model = 'const' if scores == 0 else 'linear' if scores == 1 else 'exp'