from sklearn.preprocessing import LabelBinarizer
from NNGD import *
from NNFunctions import *
from NeuralNetwork import *
import pandas as pd
from sklearn.datasets import load_digits
import math
import keras

class Ada_builder:
    def __init__(self, epsis, gammas, dataset):
        if dataset == 'mnist_toy':
            X, y = load_digits(return_X_y=True)
            cut = math.ceil(len(X) * 0.8)
            x_train = X[:cut]
            y_train = y[:cut]
            x_test = X[cut:]
            y_test = y[cut:]
        elif dataset == 'mnist':
            from keras.datasets import mnist
            (x_train, y_train), (x_test, y_test) = mnist.load_data()
            x_train = x_train.reshape(60000, 784)
            x_test = x_test.reshape(10000, 784)
            x_train = x_train.astype('float32')
            x_test = x_test.astype('float32')
            x_train /= 255
            x_test /= 255
            y_train = keras.utils.to_categorical(y_train, 10)
            y_test = keras.utils.to_categorical(y_test, 10)
        elif dataset =='and':
            train = pd.read_csv('and_train.csv')
            valid = pd.read_csv('and_valid.csv')
            x_train = train[['a', 'b']].values
            y_train = train['c'].values
            x_test = valid[['a', 'b']].values
            y_test = valid['c'].values
            y_train = keras.utils.to_categorical(y_train, 2)
            y_test = keras.utils.to_categorical(y_test, 2)
        self.train = [[img, label] for img, label in zip(x_train, y_train)]
        self.valid = [[img, label] for img, label in zip(x_test, y_test)]

        self.results = np.empty((len(epsis), len(gammas)), dtype=object)
        for i in range(len(epsis)):
            for j in range(len(gammas)):
                self.results[i, j] = []
        self.epsis = epsis
        self.gammas = gammas

    def run(self, seeds, epoch, mini_batch, layers,
            reg, act_func, out_func, error_func, w_init):
        for i, seed in enumerate(seeds):
            print("start run N°{0}".format(i))
            gene = np.random.RandomState(seed)
            train = self.train
            valid = self.valid
            gene.shuffle(train)

            for j in range(len(self.epsis)):
                for k in range(len(self.gammas)):
                    gamma = self.gammas[k]
                    epsi = self.epsis[j]
                    kf = ADADELTA(layers, 0.1, 1e-5, reg)
                    nn = NeuralNetwork(layers, act_func=act_func,
                                       out_func=out_func,
                                       error_func=error_func,
                                       w_init=w_init, gd_method=kf,
                                       rand_gen=gene)
                    metrics = nn.fit(train, epoch, mini_batch, valid)
                    self.results[j, k].append(metrics[1][-1])


    def output_csv(self, filename):
        mean_var_data = []
        for i, epsi in enumerate(self.epsis):
            for j, gamma in enumerate(self.gammas):
                mean = np.mean(self.results[i, j])
                var = np.var(self.results[i, j], ddof=1)
                mean_var_data.append([epsi, gamma, mean, var])
        df = pd.DataFrame(mean_var_data,
                          columns=['epsi', 'gamma', 'mean', 'var'])
        df.to_csv('./csv/mean_var_'+str(filename))
