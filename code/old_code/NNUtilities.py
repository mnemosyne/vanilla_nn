from sklearn import preprocessing
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


def load_parsed_data(filename):
    data = np.load('./parsed_data/nn_{0}_data.npy'
                   .format(filename))
    target = np.load('./parsed_data/nn_{0}_target.npy'
                     .format(filename))
    return data, target


def parse_dataset(filename, scaler, target_column,
                  classes, binarize, *args, **kwargs):
    try:
        data = pd.read_csv('./datasets/{0}'
                           .format(filename), *args, **kwargs)
    except IOError:
        print('IOError')
        return pd.DataFrame()
    target = data[target_column]
    data = data.drop(target_column, 1)
    if scaler == 'min_max':
        scal = preprocessing.MinMaxScaler()
        data = scal.fit_transform(data)
    if binarize:
        target = [classes[t] for t in target]
    np.save('./parsed_data/nn_{0}_data.npy'
            .format(filename), data)
    np.save('./parsed_data/nn_{0}_target.npy'
            .format(filename), target)

def plot_diff_between_models(**models):
    fig = plt.figure(1)
    metrics = ['Accuracy difference between valid and training set',
               'Cost difference between valid and training set']
    ylabel = 'Square difference'
    for i in range(len(metrics)):
        for model, score in models.items():
            ax = fig.add_subplot(len(metrics), 1, i+1)
            ax.grid(True)
            ax.set_title(metrics[i])
            ax.set_ylabel(ylabel)
            print(model)
            ax.plot(range(len(score[i])), score[i])
        ax.legend(models.keys())
    plt.show()

# Data format for plot_models(models, metrics)
# models = ['model1': [[metric1_training, metric1_valid], ..., [metricN_training, metricN_valid]],...
#          'modelN': [[metric1_training, metric1_valid], ..., [metricN_training, metricN_valid]].
# metrics = ['Name1, ... , NameN]


def plot_models(models, metrics):
    fig = plt.figure()
    for i in range(len(metrics)):
        legend = []
        for model, score in models.items():
            ax = fig.add_subplot(len(metrics), 1, i+1)
            ax.grid(True)
            ax.set_title(metrics[i])
            ax.plot(range(len((score[i])[0])), (score[i])[0])
            ax.plot(range(len((score[i])[1])), (score[i])[1])
            ax.set_xlabel('epoch')
            legend.append('Training_{0}'.format(model))
            legend.append('Validation__{0}'.format(model))
    ax.legend(legend, bbox_to_anchor=(1, 0), ncol=2)
    plt.show()

def plot_df(df, epoch):
    f, ax = plt.subplots(figsize=(9, 6))
    plt.title("Mean of the score by entry for mnist after {0} epoch \n"
              "in function of adadelta hyper-parameters".format(epoch))
    sns.heatmap(df, annot=True, fmt='.3f', ax=ax, cmap='magma_r', robust=True)
    plt.show()
