from functools import partial


class BasicChooser:
    def __init__(self, values, trials=None):
        self.values = values
        self.next = 0
        self.trials = trials

    def next_value(self):
        val = self.values[self.next]
        self.next = self.next+1 if self.next < len(self.values) - 1 else 0
        if self.trials:
            self.trials -= 1
        return val

    def finish(self):
        if self.trials is None:
            return False
        else:
            return self.trials == 0


class ReversePowChooser:
    def __init__(self, upper, lower, trials=None):
        self.upper = upper
        self.current = upper
        self.lower = lower
        self.trials = trials

    def next_value(self):
        val = 1 - 10**-int(self.current)
        self.current = self.current - 1 if self.current > self.lower else self.upper
        if self.trials:
            self.trials -= 1
        return val

    def finish(self):
        if self.trials is None:
            return False
        else:
            return self.trials == 0

class LawBasedChooser:
    def __init__(self, law, params, trials=None):
        self.value = partial(law, params)
        self.trials = trials

    def next_value(self):
        if self.trials:
            self.trials -= 1
        return self.value()

    def finish(self):
        if self.trials is None:
            return False
        else:
            return self.trials == 0


