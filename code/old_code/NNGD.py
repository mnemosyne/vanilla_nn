import numpy as np
import sys

class KALMAN:
    def __init__(self, layers, reg):
        self.w_g_V = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.w_h_V = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.b_g_V = [np.zeros((y, 1), dtype=float) for y in layers[1:]]
        self.b_h_V = [np.zeros((y, 1), dtype=float) for y in layers[1:]]

        self.w_g_m = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.w_h_m = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.b_g_m = [np.zeros((y, 1), dtype=float) for y in layers[1:]]
        self.b_h_m = [np.zeros((y, 1), dtype=float) for y in layers[1:]]

        self.w_V_g = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.w_V_h = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.b_V_g = [np.zeros((y, 1), dtype=float) for y in layers[1:]]
        self.b_V_h = [np.zeros((y, 1), dtype=float) for y in layers[1:]]

        self.d_w = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.d_b = [np.zeros((y, 1), dtype=float) for y in layers[1:]]
        self.w_g1 = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.b_g1 = [np.zeros((y, 1), dtype=float) for y in layers[1:]]
        self.w_h1 = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.b_h1 = [np.zeros((y, 1), dtype=float) for y in layers[1:]]
        self.reg = reg
        self.count = 0

    def train(self, weights, w_grad, biases, b_grad, nb_entries):
        self.count += 1
        print("train no{0}".format(self.count))
        t = self.count
        w_g_V_t = self.w_g_V.copy()
        b_g_V_t = self.b_g_V.copy()
        w_h_V_t = self.w_h_V.copy()
        b_h_V_t = self.b_h_V.copy()
        self.w_g_V = [w_g_V + d_w**2 * w_h_V for w_g_V, d_w, w_h_V
                      in zip(self.w_g_V, self.d_w, self.w_h_V)]
        self.b_g_V = [b_g_V + d_b**2 * b_h_V for b_g_V, d_b, b_h_V
                      in zip(self.b_g_V, self.d_b, self.b_h_V)]

        self.w_h_V = [w_h_V + w_V_h for w_h_V, w_V_h in zip(self.w_h_V, self.w_V_h)]
        self.b_h_V = [b_h_V + b_V_h for b_h_V, b_V_h in zip(self.b_h_V, self.b_V_h)]
        self.w_g_m = [w_g_m + w_h_m * d_w for w_g_m, w_h_m, d_w in zip(self.w_g_m, self.w_h_m, self.d_w)]
        self.b_g_m = [b_g_m + b_h_m * d_b for b_g_m, b_h_m, d_b in zip(self.b_g_m, self.b_h_m, self.d_b)]

        for i, (vars, grads) in enumerate(zip(self.w_V_g, w_grad)):
            for j, (var, grad) in enumerate(zip(vars, grads)):
                for k in range(len(var)):
                    self.w_V_g[i][j][k] = var[k] if var[k] > 0 else grad[k]**2 + 1e-15
        for i, (vars, grads) in enumerate(zip(self.b_V_g, b_grad)):
            for j, (var, grad) in enumerate(zip(vars, grads)):
                for k in range(len(var)):
                    self.b_V_g[i][j][k] = var[k] if var[k] > 0 else grad[k]**2 + 1e-15

        for i, var in enumerate(self.w_g_V):
            for j, v in enumerate(var):
                for k in range(len(v)):
                    w_g_V_t[i][j][k] = v[k] if v[k] > 0 and self.count > 1 else 0
        for i, var in enumerate(self.b_g_V):
            for j, v in enumerate(var):
                for k in range(len(v)):
                    b_g_V_t[i][j][k] = v[k] if v[k] > 0 and self.count > 1 else 0

        self.w_g_V = [1/(1 / w_V_g + w_g_V_t) for w_V_g, w_g_V_t in zip(self.w_V_g, w_g_V_t)]
        self.b_g_V = [1/(1 / b_V_g + b_g_V_t) for b_V_g, b_g_V_t in zip(self.b_V_g, b_g_V_t)]

        for i, var in enumerate(self.w_h_V):
            for j, v in enumerate(var):
                for k in range(len(v)):
                    w_h_V_t[i][j][k] = 1/v[k] if v[k] > 0 and self.count > 1 else 0
        for i, var in enumerate(self.b_h_V):
            for j, v in enumerate(var):
                for k in range(len(v)):
                    b_h_V_t[i][j][k] = 1/v[k] if v[k] > 0 and self.count > 1 else 0

        w_h_V_t = [0.5*w_d**2 / w_V_g + w_h_V_t for w_d, w_V_g, w_h_V_t
                             in zip(self.d_w, self.w_V_g, w_h_V_t)]
        b_h_V_t = [0.5*b_d**2 / b_V_g + b_h_V_t for b_d, b_V_g, b_h_V_t
                             in zip(self.d_b, self.b_V_g, b_h_V_t)]
        for i, var in enumerate(w_h_V_t):
            for j, v in enumerate(var):
                for k in range(len(v)):
                    self.w_h_V[i][j][k] = 1/v[k] if v[k] > 0 and self.count > 1 else 0
        for i, var in enumerate(b_h_V_t):
            for j, v in enumerate(var):
                for k in range(len(v)):
                    self.b_h_V[i][j][k] = 1/v[k] if v[k] > 0 and self.count > 1 else 0

        self.w_g_m = [w_g_V * (w_g / w_V_g + w_g_V_t * w_g_m)
                      for w_g_V, w_g, w_V_g, w_g_V_t, w_g_m in zip(self.w_g_V, w_grad, self.w_V_g, w_g_V_t, self.w_g_m)]
        self.b_g_m = [b_g_V * (b_g / b_V_g + b_g_V_t * b_g_m)
                      for b_g_V, b_g, b_V_g, b_g_V_t, b_g_m in zip(self.b_g_V, b_grad, self.b_V_g, b_g_V_t, self.b_g_m)]

        self.w_h_m = [w_h_V * (0.5*(w_g - w_g1) * d_w / w_V_g + w_h_V_t*w_h_m)
                      for w_h_V, w_g, w_g1, d_w, w_V_g, w_h_V_t, w_h_m
                      in zip(self.w_h_V, w_grad, self.w_g1, self.d_w, self.w_V_g, w_h_V_t, self.w_h_m)]
        self.b_h_m = [b_h_V * (0.5 * (b_g - b_g1) * d_b / b_V_g + b_h_V_t * b_h_m)
                      for b_h_V, b_g, b_g1, d_b, b_V_g, b_h_V_t, b_h_m
                      in zip(self.b_h_V, b_grad, self.b_g1, self.d_b, self.b_V_g, b_h_V_t, self.b_h_m)]
        self.w_V_g = [w_V_g + ((w_g - w_g_m)**2 - w_V_g)/t
                      for w_V_g, w_g, w_g_m in zip(self.w_V_g, w_grad, self.w_g_m)]
        self.b_V_g = [b_V_g + ((b_g - b_g_m)**2 - b_V_g)/t
                      for b_V_g, b_g, b_g_m in zip(self.b_V_g, b_grad, self.b_g_m)]
        self.w_V_h = [w_V_h + ((w_h_m - w_h1)**2 - w_V_h)/t
                      for w_V_h, w_h_m, w_h1 in zip(self.w_V_h, self.w_h_m, self.w_h1)]
        self.b_V_h = [b_V_h + ((b_h_m - b_h1)**2 - b_V_h)/t
                      for b_V_h, b_h_m, b_h1 in zip(self.b_V_h, self.b_h_m, self.b_h1)]

        for i, (w_g_m, w_h_m) in enumerate(zip(self.w_g_m, self.w_h_m)):
            for j, (w_g, w_h) in enumerate(zip(w_g_m, w_h_m)):
                for k in range(len(w_g)):
                    self.d_w[i][j][k] = 0 if w_h[k] == 0 else -w_g[k]/w_h[k]
        for i, (b_g_m, b_h_m) in enumerate(zip(self.b_g_m, self.b_h_m)):
            for j, (b_g, b_h) in enumerate(zip(b_g_m, b_h_m)):
                for k in range(len(b_g)):
                    self.d_b[i][j][k] = 0 if b_h[k] == 0 else -b_g[k]/b_h[k]

        weights = [w + d_w for w, d_w in zip(weights, self.d_w)]
        biases = [b + d_b for b, d_b in zip(biases, self.d_b)]
        self.w_h1 = self.w_h_m.copy()
        self.b_h1 = self.b_h_m.copy()
        self.w_g1 = w_grad.copy()
        self.b_g1 = b_grad.copy()
        return weights, biases

class SGD:
    def __init__(self, lr, reg):
        self.lr = lr
        self.reg = reg

    def to_string(self):
        return 'SGD(lr={0}) ' \
               'reg={1} '.format(self.lr, self.reg.to_string())

    def train(self, weights, delta_w, biases, delta_b, nb_entries):
        weights = [current_w - self.lr * new_w - self.lr * self.reg.w(current_w) / nb_entries for
                   current_w, new_w in zip(weights, delta_w)]

        biases = [current_b - self.lr * new_b - self.lr * self.reg.b(current_b) / nb_entries for
                  current_b, new_b in zip(biases, delta_b)]
        return weights, biases

class ADADELTA:
    def __init__(self, layers, decay_rate, epsi, reg):
        self.decay_rate = decay_rate
        self.first_train = True
        self.lr = 1
        self.epsi = epsi
        self.reg = reg
        self.w_ac_grad = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.w_ac_delta = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.b_ac_grad = [np.zeros((y, 1), dtype=float) for y in layers[1:]]
        self.b_ac_delta = [np.zeros((y, 1), dtype=float) for y in layers[1:]]

    def to_string(self):
        return 'Adadelta=(gamma={0}, epsi={1}) ' \
               'reg={2} '.format(self.decay_rate, self.epsi, self.reg.to_string())

    def train(self, weights, delta_w, biases, delta_b, nb_entries):
        self.w_ac_grad = [self.decay_rate*w_ac_g + (1 - self.decay_rate)*(w_d**2) for w_ac_g, w_d
                       in zip(self.w_ac_grad, delta_w)]

        self.b_ac_grad = [self.decay_rate*b_ac_g + (1 - self.decay_rate)*(b_d**2) for b_d, b_ac_g
                       in zip(delta_b, self.b_ac_grad)]
        if self.first_train:
            w_update = [-1*self.lr * w_d for
                        w_ac_g, w_ac_d, w_d in zip(self.w_ac_grad, self.w_ac_delta, delta_w)]
            b_update = [-1 * self.lr * b_d for
                        b_ac_g, b_ac_d, b_d in zip(self.b_ac_grad, self.b_ac_delta, delta_b)]
            self.first_train = False
        else:
            w_update = [(-1*(w_ac_d + self.epsi)**0.5 / (w_ac_g + self.epsi)**0.5)*w_d for
                        w_ac_g, w_ac_d, w_d in zip(self.w_ac_grad, self.w_ac_delta, delta_w)]
            b_update = [(-1 * (b_ac_d + self.epsi)**0.5 / (b_ac_g + self.epsi)**0.5) * b_d for
                        b_ac_g, b_ac_d, b_d in zip(self.b_ac_grad, self.b_ac_delta, delta_b)]

        self.w_ac_delta = [self.decay_rate*w_ac_d + (1 - self.decay_rate)*(w_up**2) for w_ac_d, w_up
            in zip(self.w_ac_delta, w_update)]

        self.b_ac_delta = [self.decay_rate*b_ac_d + (1 - self.decay_rate)*(b_up**2) for b_ac_d, b_up
            in zip(self.b_ac_delta, b_update)]

        weights = [w + d_w - self.reg.w(d_w)/nb_entries for w, d_w in zip(weights, w_update)]
        biases = [b + d_b - self.reg.b(d_b)/nb_entries for b, d_b in zip(biases, b_update)]

        return weights, biases

class ADADELTA_adadecay:
    def __init__(self, layers, epsi, reg):
        self.w_rate = [np.ones((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.b_rate = [np.ones((y, 1), dtype=float) for y in layers[1:]]
        self.epsi = epsi
        self.reg = reg
        self.w_ac_grad = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.w_ac_delta = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.b_ac_grad = [np.zeros((y, 1), dtype=float) for y in layers[1:]]
        self.b_ac_delta = [np.zeros((y, 1), dtype=float) for y in layers[1:]]

    def to_string(self):
        return 'Adadelta_{0}_{1}_{2}'.format(self.decay_rate, self.epsi, self.reg.to_string())

    def train(self, weights, delta_w, biases, delta_b, nb_entries):
        self.w_ac_grad = [decay*w_ac_g + (1 - decay)*(w_d**2) for decay, w_ac_g, w_d
                       in zip(self.w_rate, self.w_ac_grad, delta_w)]

        self.b_ac_grad = [decay*b_ac_g + (1 - decay)*(b_d**2) for decay, b_d, b_ac_g
                       in zip(self.b_rate, delta_b, self.b_ac_grad)]

        w_lr = [((w_ac_d + self.epsi)**0.5 / (w_ac_g + self.epsi)**0.5) for
                    w_ac_g, w_ac_d in zip(self.w_ac_grad, self.w_ac_delta)]

        w_max = np.max([np.max(lr) for lr in w_lr]) - sys.float_info.epsilon
        w_min = np.min([np.min(lr) for lr in w_lr]) + sys.float_info.epsilon

        b_lr = [((b_ac_d + self.epsi)**0.5 / (b_ac_g + self.epsi)**0.5) for
                    b_ac_g, b_ac_d in zip(self.b_ac_grad, self.b_ac_delta)]
        b_max = np.max([np.max(lr) for lr in b_lr]) - sys.float_info.epsilon
        b_min = np.min([np.min(lr) for lr in b_lr]) + sys.float_info.epsilon

        w_update = [(-lr)*d_w for lr, d_w in zip(w_lr, delta_w)]
        b_update = [(-lr)*d_b for lr, d_b in zip(b_lr, delta_b)]

        self.w_ac_delta = [decay*w_ac_d + (1 - decay)*(w_up**2) for decay, w_ac_d, w_up
            in zip(self.w_rate, self.w_ac_delta, w_update)]

        self.b_ac_delta = [decay*b_ac_d + (1 - decay)*(b_up**2) for decay, b_ac_d, b_up
            in zip(self.b_rate, self.b_ac_delta, b_update)]
        self.w_rate = [(y - w_max) / (w_min - w_max) for y in w_lr]
        self.b_rate = [(y - b_max) / (b_min - b_max) for y in b_lr]

        weights = [w + d_w - self.reg.w(d_w)/nb_entries for w, d_w in zip(weights, w_update)]
        biases = [b + d_b - self.reg.b(d_b)/nb_entries for b, d_b in zip(biases, b_update)]

        return weights, biases