from WeightInitializer import *
from WeightRegression import *
from mnemonas.mnemonas import CurveFit
import time
import sys

import matplotlib.pyplot as plt
import seaborn as sn
"""
Vanilia neural network:


layers = list of the number of neurones by layers 
[input, hidden1, ..., hiddenN, output]
error_func = error/cost function
act_func = activation function
out_func = output function
gb_method = gradient descent method
w_init = weight initializer method
reg = regression method 
dropout_prob = proportion of activated weights by layer

"""

"""
def sgd_bb(self, init_step_size, max_epoch, train_data, valid_data, mini_batch_size=None, beta=None,
           verbose=True, accuracy=True, cost=True):
    valid_acc = []
    train_acc = []
    valid_cost = []
    train_cost = []

        SGD with Barzilai-Borwein step size for solving finite-sum problems
        grad: gradient function in the form of grad(x, idx), where idx is a list of induces
        init_step_size: initial step size
        n, d: size of the problem
        m: step sie updating frequency
        beta: the averaging parameter
        phi: the smoothing function in the form of phi(k)
        func: the full function, f(x) returning the function value at x
    m = mini_batch_size
    n = len(train_data)
    if not isinstance(m, int) or m <= 0:
        m = n
        if verbose:
            print('Info: set m=n by default')

    if not isinstance(beta, float) or beta <= 0 or beta >= 1:
        beta = 10 / m
        if verbose:
            print('Info: set beta=10/m by default')
    step_size_weights = init_step_size
    step_size_biases = init_step_size
    # c = 1
    for k in range(max_epoch):
        weights_tilde = self.weights.copy()
        biases_tilde = self.biases.copy()
        # estimate step size by BB method
        if k > 1:
            print("enter")
            s_weights = [(w_tilde - l_w_tilde).ravel() for w_tilde, l_w_tilde in zip(weights_tilde, last_weights_tilde)]
            s_weights = np.concatenate(s_weights).ravel()
            s_biases = [(b_tilde - l_b_tilde).ravel() for b_tilde, l_b_tilde in zip(biases_tilde, last_biases_tilde)]
            s_biases = np.concatenate(s_biases).ravel()
            y_weights = [(w_hat - l_w_hat).ravel() for w_hat, l_w_hat in zip(grad_hat_weights, last_grad_hat_weights)]
            y_weights = np.concatenate(y_weights).ravel()
            y_biases = [(b_hat - l_b_hat).ravel() for b_hat, l_b_hat in zip(grad_hat_biases, last_grad_hat_biases)]
            y_biases = np.concatenate(y_biases).ravel()
            m_invert = 1 / m

            # by dimension estimation
            print(s_weights)
            print(y_weights)
            print(s_biases)
            print(y_biases)
            step_size_weights = m_invert*(np.linalg.norm(s_weights) ** 2 /abs(np.dot(s_weights, y_weights)))
            step_size_biases = m_invert*(np.linalg.norm(s_biases) ** 2 / abs(np.dot(s_biases, y_biases)))
            f phi is not None:
                c = c ** ((k - 2) / (k - 1)) * (step * phi(k)) ** (1 / (k - 1))
                step_size_weights = c / phi(k)

        if verbose:
            print("total grad")
            grads = [self.compute_gradient(entry) for entry in train_data]
            w, b = np.sum(grads, axis=0)
            output = 'Epoch.: %d, Step size: %.2e, Grad. norm: %.2e' % \
                     (k, step_size, np.linalg.norm(w) + np.linalg.norm(b))
            print(output)

        if k > 0:
            last_grad_hat_weights = grad_hat_weights
            last_grad_hat_biases = grad_hat_biases
            last_weights_tilde = weights_tilde
            last_biases_tilde = biases_tilde
        if k == 0:
            grad_hat_weights = [np.zeros(w.shape) for w in self.weights]
            grad_hat_biases = [np.zeros(b.shape) for b in self.biases]

        # core logic
        ids = self.rand_g.randint(n, size=m)
        print(ids)
        print("pre computirng")
        grads = [self.compute_gradient(train_data[id]) for id in ids]
        weights_grad, biases_grad = np.sum(grads, axis=0)
        weights_grad /= m
        biases_grad /= m
        print(weights_grad)
        print("post computirng")
        print(step_size_weights)
        print(step_size_biases)
        self.weights = [w - step_size_weights * g for w, g in zip(self.weights, weights_grad)]
        self.biases = [b - step_size_biases * g for b, g in zip(self.biases, biases_grad)]
        # average the gradients
        grad_hat_weights = [beta * g + (1 - beta) * g_hat for g, g_hat in
                            zip(weights_grad, grad_hat_weights)]
        grad_hat_biases = [beta * g + (1 - beta) * g_hat for g, g_hat in
                               zip(biases_grad, grad_hat_biases)]
        print("end core logic")
        if verbose:
            print("Epoch n°{0}".format(k))
        if valid_data:
            print("validation score computation")
            if accuracy:
                valid_prec = self.accuracy_class(valid_data)
                train_prec = self.accuracy_class(train_data)
                # self.model_fit.add(valid_prec)
                valid_acc.append(valid_prec)
                train_acc.append(train_prec)
                if verbose:
                    print("Classification scores:\n"
                          "train set = {0}\n"
                          "test set = {1}\n"
                          .format(train_prec, valid_prec))

            if cost:
                v_cost = self.cost(valid_data)
                t_cost = self.cost(train_data)
                valid_cost.append(v_cost)

                train_cost.append(t_cost)
                if verbose:
                    print("Cost scores:\n"
                          "train set = {0}\n"
                          "test set = {1}\n"
                      .format(t_cost, v_cost))
    return valid_acc, train_acc, valid_cost, train_cost
"""

class NeuralNetwork:
    def __init__(self, layers, error_func, act_func,
                 out_func, gd_method, w_init=standard_initializer,
                 rand_gen=np.random.RandomState()):
        t = time.time()
        self.e_func = error_func
        self.a_func = act_func
        self.o_func = out_func
        self.layers = layers
        self.weights, self.biases = w_init(layers, rand_gen)
        self.gd_method = gd_method
        self.reg = gd_method.reg
        self.rand_g = rand_gen
        self.model_fit = CurveFit()
        print("NN initial phase: Ok {0}s".format(time.time() -t))

    """
    compute the error gradient for the cost function
    """
    def to_string(self):
        to_string_layers = '('
        for x in self.layers[:-1]:
            to_string_layers += str(x)+','
        to_string_layers += str(self.layers[-1])+')'
        return 'NN parameters: ' \
               'layers={0} ' \
               'error={1} ' \
               'activation={2} ' \
               'output={3} ' \
               'method={4}'.format(to_string_layers, self.e_func['name'],
                                                self.a_func['name'], self.o_func['name'],
                                                self.gd_method.to_string())

    def error_grad(self, labels, pre_act, act):
        if self.e_func['name'] == 'quadratic':
            return self.e_func['deriv'](act[-1], labels) \
                   * self.o_func['deriv'](pre_act[-1])
        if self.e_func['name'] == 'cross_entropy':
            return self.e_func['deriv'](act[-1], labels)
        """
        predict y from x
        """
    def predict(self, x):
        x = x[:, np.newaxis]
        for w, b in zip(self.weights[:-1], self.biases[:-1]):
            x = self.a_func['id'](np.dot(w, x) + b)
        return self.o_func['id'](np.dot(self.weights[-1], x)
                                 + self.biases[-1]).ravel()
    """
    Compute the total gradient for each weights and biases.
    """
    def compute_gradient(self, data):
        X = data[0]
        y = data[1]

        grad_w = [np.empty(weight.shape, dtype=float) for weight in self.weights]
        grad_b = [np.empty(biase.shape, dtype=float) for biase in self.biases]

        act = [np.empty((x, 1), dtype=float) for x in self.layers]
        pre_act = [np.empty((x, 1), dtype=float) for x in self.layers[1:]]

        act[0] = X[:, np.newaxis]
        y = y[:, np.newaxis]

        layer_count = 1
        for w, b in zip(self.weights[:-1], self.biases[:-1]):
            pre_act[layer_count - 1] = np.dot(w, act[layer_count-1]) + b
            act[layer_count] = self.a_func['id'](pre_act[layer_count-1])
            layer_count += 1
        pre_act[layer_count - 1] = \
            np.dot(self.weights[-1], act[layer_count - 1]) + self.biases[-1]

        act[layer_count] = self.o_func['id'](pre_act[layer_count - 1])
        delta = self.error_grad(y, pre_act, act)

        grad_b[-1] = delta
        grad_w[-1] = np.dot(delta, act[-2].T)
        for i in range(2, len(self.layers)):
            delta = np.dot(self.weights[-i+1].T, delta) \
                    * self.a_func['deriv'](pre_act[-i])

            grad_b[-i] = delta
            grad_w[-i] = np.dot(delta, act[-i-1].T)
        return grad_w, grad_b


    def fit(self, train_data, epoch, mini_batch, valid_data=None, accuracy=False, cost=False, output=True,
            line_search=False):


        nb_entries = len(train_data)
        """
        if bb:
            valid_acc, train_acc, valid_cost, train_cost = \
                self.sgd_bb(0.5, epoch, train_data, valid_data, mini_batch, 0.01)
        else:
        """
        valid_acc = []
        train_acc = []
        valid_cost = []
        train_cost = []
        j = 0
        while j < epoch or self.model_fit.getMode() != 'c':
            print(self.model_fit.asString())
            self.rand_g.shuffle(train_data)
            for i in range(0, len(train_data), mini_batch):
                score_init = self.cost(valid_data)
                acc_init = self.accuracy_class(valid_data)
                weights_temp = self.weights.copy()
                biases_temp = self.biases.copy()
                print("{0} for {1}".format(i/mini_batch, len(train_data)/mini_batch))
                self.weights, self.biases = self.fit_mini_batch(train_data[i:i + mini_batch], nb_entries)
                new_score = self.cost(valid_data)
                new_acc = self.accuracy_class(valid_data)
                print(acc_init)
                print(new_acc)
                if score_init <= new_score and line_search:
                    print('First order.')
                    self.weights = weights_temp.copy()
                    self.biases = biases_temp.copy()
                    find_better = False
                    ld = 1
                    w_sq = 0
                    b_sq = 0
                    for i in range(0, len(train_data), 10):
                        print(i/len(train_data))
                        grad_w = []
                        grad_b = []
                        for entry in train_data[i:i+10]:
                            grad_w_t, grad_b_t = self.compute_gradient(entry)
                            grad_w += grad_w_t
                            grad_b += grad_b_t
                            w_sq += np.sum([np.sum(w * w) for w in grad_w])
                            b_sq += np.sum([np.sum(b * b) for b in grad_b])
                    l0 = self.cost(train_data)
                    while not find_better:
                        print('first')
                        weights_temp = self.weights.copy()
                        biases_temp = self.biases.copy()
                        acc_init = self.accuracy_class(valid_data)
                        w_d = [l0*ld*g_w/w_sq for g_w in grad_w]
                        b_d = [l0*ld*g_b/w_sq for g_b in grad_b]
                        print(w_d)
                        print('weights total gradient magnitude {0}'.format(w_sq))
                        print('Biases total gradient magnitude {0}'.format(b_sq))
                        self.weights = [w - w_d for w, w_d in zip(self.weights, w_d)]
                        self.biases = [b - b_d for b, b_d in zip(self.biases, b_d)]
                        new_acc = self.accuracy_class(valid_data)
                        print('Old score = {0}'.format(acc_init))
                        print('New score {0}'.format(new_acc))
                        if new_acc > acc_init:
                            find_better = True
                        else:
                            self.weights = weights_temp
                            self.biases = biases_temp
                            ld *= 0.5
                        print('lambda = {0}'.format(ld))

            if output:
                print("Epoch n°{0}".format(j))
            if valid_data:
                if accuracy:
                    valid_prec = self.accuracy_class(valid_data)
                    train_prec = self.accuracy_class(train_data)
                    self.model_fit.add(valid_prec)
                    valid_acc.append(valid_prec)
                    train_acc.append(train_prec)
                    if output:
                        print("Classification scores:\n"
                              "train set = {0}\n"
                              "test set = {1}\n"
                              .format(train_prec, valid_prec))

                if cost:
                    v_cost = self.cost(valid_data)
                    t_cost = self.cost(train_data)
                    valid_cost.append(v_cost)
                    train_cost.append(t_cost)
                    if output:
                        print("Cost scores:\n"
                              "train set = {0}\n"
                              "test set = {1}\n"
                              .format(t_cost, v_cost))
            j += 1

        # if output:
        #    self.print_result(epoch, valid_acc, train_acc, valid_cost, train_cost)
        train_acc = np.array(train_acc)
        valid_acc = np.array(valid_acc)
        train_cost = np.array(train_cost)
        valid_cost = np.array(valid_cost)
        square_diff_acc = (train_acc - valid_acc)**2
        square_diff_cost = (train_cost - valid_cost)**2
        return train_acc, valid_acc, square_diff_acc, \
               train_cost, valid_cost, square_diff_cost

    def fit_mini_batch(self, mini_batch, nb_entries):
        print("in fit mini batch")
        # compute the gradient descent for the all sample
        len_mb = len(mini_batch)
        grad_w = []
        grad_b = []
        for entry in mini_batch:
            grad_w_t, grad_b_t = self.compute_gradient(entry)
            grad_w += grad_w_t
            grad_b += grad_b_t
        grad_w = np.array(grad_w)
        grad_b = np.array(grad_b)
        grad_w /= len_mb
        grad_b /= len_mb
        print("out fit mini batch")
        return self.gd_method.train(self.weights, grad_w, self.biases, grad_b, nb_entries)

    # sum of the scores of the dataset + regularisation of weights
    # the final cost function to minimise is J(X) = C(X) + R(X)
    # where C(X) is the cost function and R(X) the regularisation method
    def cost(self, dataset):
        cost = [self.e_func['id'](self.predict(set[0]), set[1]) for set in dataset]
        cost = np.sum(cost)
        reg = self.reg.cost(self.weights, self.biases)
        return cost + len(dataset)*reg

    def accuracy_class(self, dataset):
        accuracy = 0
        for set in dataset:
            pred = self.predict(set[0])
            arg_pred = np.argmax(pred)
            arg_label = np.argmax(set[1])
            if arg_pred == arg_label:
                accuracy += 1
        return accuracy/len(dataset)

    def save(self, filename):
        np.save('./networks/nn_{filename}_weights.npy'.format(filename=filename), self.weights)
        np.save('./networks/nn_{filename}_biases.npy'.format(filename=filename), self.biases)

    def load(self, filename):
        self.weights = np.load('./networks/nn_{filename}_weights.npy'.format(filename=filename))
        self.biases = np.load('./networks/nn_{filename}_biases.npy'.format(filename=filename))

    def print_result(self, epoch, valid_acc, train_acc, valid_cost, train_cost):
        fig = plt.figure(1)
        ax = fig.add_subplot(211)
        ax.plot(range(epoch), valid_acc)
        ax.plot(range(epoch), train_acc)
        ax.set_xlim([0, epoch])
        ax.grid(True)
        ax.set_title('Classification accuracy\n{nb} classes'.format(nb=self.layers[-1]))
        ax.set_ylabel('Accuracy')
        ax.set_xlabel('epoch')
        ax.legend(['Validation', 'training'])

        ax = fig.add_subplot(212)
        ax.plot(range(epoch), valid_cost)
        ax.plot(range(epoch), train_cost)
        ax.set_xlim([0, epoch])
        ax.grid(True)
        ax.set_title('Error score: {function}'.format(function=self.e_func['name']))
        ax.set_ylabel('Cost')
        ax.set_xlabel('epoch')
        ax.legend(['Validation', 'training'])
        fig.suptitle(self.to_string())
        fig.subplots_adjust(top=0.863, bottom=0.066, left=0.055, right=0.99, hspace=0.279, wspace=0.0)

        fig.tight_layout()
        plt.show()
