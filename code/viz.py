import matplotlib.pyplot as plt
import pandas as pd
from scipy.interpolate import griddata
import numpy as np
import seaborn as sns
sns.set(color_codes=True)
import scipy
df1 = pd.read_csv('/home/romain/ENS_A1/Stage_mnenosyne/code/vanilla_nn/code/loss_valid.csv')
df1.drop(['epsi','gamma'], 1, inplace=True)
df1['group'] = 'ADADELTA'
df1.columns = ['epoch', 'loss', 'group']
df2 = pd.read_csv('/home/romain/ENS_A1/Stage_mnenosyne/code/vanilla_nn/code/loss_valid_kf.csv')
df2['group'] = 'KALMAN_FILTER'
df2.columns = ['epoch', 'loss', 'group']
df = pd.concat([df1, df2], ignore_index=True)
sns.relplot(x="epoch", y="loss", hue='group', kind="line", data=df)
plt.title('ADADELTA vs KALMAN FILTER validation quadratic loss on MNIST classification')
plt.show()
"""
gamma_g = np.arange(0.1, 0.9, 0.1)
epsi_g = [1e-15, 1e-14, 1e-13, 1e-12, 1e-11, 1e-10, 1e-9,
        1e-8, 1e-7, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1]

intvx = [(0, 0.1), (0.1, 0.2), (0.2, 0.3), (0.3, 0.4),(0.4, 0.5), (0.5, 0.6),
           (0.6, 0.7), (0.7, 0.8), (0.8, 0.9), (0.9, 1.1)]
binx = pd.IntervalIndex.from_tuples(intvx)
intvy = [(1.1e-16, 1e-15), (1e-15, 1e-14), (1e-14, 1e-13), (1e-13, 1e-12), (1e-12, 1e-11), (1e-11, 1e-10),
                 (1e-10, 1e-9), (1e-9, 1e-8), (1e-8, 1e-7), (1e-7, 1e-6), (1e-6, 1e-5), (1e-5, 1e-4),
                 (1e-4, 1e-3), (1e-3, 1e-2), (1e-2, 1.1e-1)]
biny = pd.IntervalIndex.from_tuples(intvy)
df = pd.read_csv('/home/rferrand/PycharmProjects/vanilla_nn/code/csv/parsed.csv', index_col=0)
df['gamma_g'] = pd.cut(df.gamma, binx, right=True,
                           retbins=False, precision=3, include_lowest=True, duplicates='raise', )
df['epsi_g'] = pd.cut(df.epsi, biny, right=True,
                           retbins=False, precision=3, include_lowest=True, duplicates='raise')

grp = df.groupby(['gamma_g', 'epsi_g']).mean()
pivot = grp.pivot_table(index='epsi_g', columns='gamma_g', values='mean')
f, ax = plt.subplots(figsize=(9, 6))
plt.suptitle('Accuracy on validation dataset '
          'for Adadelta in function of its hyper-parameters')

sns.heatmap(pivot,  annot=True, fmt='.2f', ax=ax, cmap='magma',
            robust=True, cbar_kws={'label': 'Accuracy'})
plt.xlabel("Gamma groups")
plt.xticks(rotation=45)
plt.ylabel("Epsilon groups")

x = df.gamma.values
x_class = df.gamma_g
y = df.epsi_g
z = df['mean'].values

plt.figure()
sns.countplot(x_class)
plt.suptitle('Gamma classes distribution')
plt.xlabel("Gamma groups")
plt.xticks(rotation=45)
plt.figure()
sns.countplot(y)
plt.suptitle('Epsilon classes distribution')
plt.xlabel("Epsilon groups")
plt.xticks(rotation=45)
plt.figure()
sns.violinplot(x=x_class, y=z,
               split=True, inner="stick", palette="Set3", cut=0.)
plt.suptitle('Accuracy on validation set in function of the gamma group')
plt.ylabel("Accuracy on validation set")
plt.xlabel("Gamma groups")
plt.xticks(rotation=45)

plt.figure()
sns.violinplot(x=y, y=z,
               split=True, inner="stick", palette="Set3", cut=0.)
plt.suptitle('Accuracy on validation set in function of the epsilon group')
plt.ylabel("Accuracy on validation set")
plt.xlabel("Epsilon groups")
plt.xticks(rotation=45)
plt.show()
"""