from WeightInitializer import *
from WeightRegression import *
import time
import sys
import matplotlib.pyplot as plt

class NeuralNetwork:
    def __init__(self, layers, error_func, act_func,
                 out_func, gd_method, w_init=standard_initializer,
                 rand_gen=np.random.RandomState(), debug = False):
        t = time.time()
        self.e_func = error_func
        self.a_func = act_func
        self.o_func = out_func
        self.layers = layers
        self.weights, self.biases = w_init(layers, rand_gen)
        self.gd_method = gd_method
        self.rand_g = rand_gen
        self.acc_score_train = []
        self.acc_score_valid = []
        self.loss_score_train = []
        self.loss_score_valid = []
        self.debug = debug

        print("NN initial phase: Ok {0}s".format(time.time() -t))

    """
    Output string representation of the model
    """
    def to_string(self):
        to_string_layers = '('
        for x in self.layers[:-1]:
            to_string_layers += str(x)+','
        to_string_layers += str(self.layers[-1])+')'
        return 'NN parameters: ' \
               'layers={0} ' \
               'error={1} ' \
               'activation={2} ' \
               'output={3} ' \
               'method={4}'.format(to_string_layers, self.e_func['name'],
                                                self.a_func['name'], self.o_func['name'],
                                                self.gd_method.to_string())

    """
    compute the error gradient for the cost function
    """
    def error_grad(self, labels, pre_act, act):
        if self.e_func['name'] == 'quadratic':
            return self.e_func['deriv'](act[-1], labels) \
                   * self.o_func['deriv'](pre_act[-1])
        if self.e_func['name'] == 'cross_entropy':
            return self.e_func['deriv'](act[-1], labels)
    """
    predict y from x
    """
    def predict(self, x):
        for w, b in zip(self.weights[:-1], self.biases[:-1]):
            x = self.a_func['id'](np.dot(w, x).ravel() + b)

        x = self.o_func['id'](np.dot(self.weights[-1], x).ravel() + self.biases[-1])
        return x
    """
    Compute the gradient for each weights and biases.
    """
    def compute_gradient(self, datas):
        grads = []
        grad_w = [np.empty(weight.shape, dtype=float) for weight in self.weights]
        grad_b = [np.empty(biase.shape, dtype=float) for biase in self.biases]

        for data in datas:
            x = data[0]
            y = data[1]
            pre_acts = []
            acts = [x]
            act = x
            # feed forward
            for w, b in zip(self.weights[:-1], self.biases[:-1]):
                pre_act = np.dot(w, act).ravel() + b
                pre_acts.append(pre_act)
                act = self.a_func['id'](pre_act)
                acts.append(act)
            pre_act = np.dot(self.weights[-1], act).ravel() + self.biases[-1]
            pre_acts.append(pre_act)
            act = self.o_func['id'](pre_act)
            acts.append(act)
            # backpropagation
            delta = self.error_grad(y, pre_acts, acts)
            grad_b[-1] = delta
            if isinstance(delta, float):
                d = delta
            else:
                d = delta[:, np.newaxis]
            grad_w[-1] = acts[-2] * d
            for i in range(2, len(self.layers)):

                delta = np.dot(np.transpose(self.weights[-i+1]), delta) \
                        * self.a_func['deriv'](pre_acts[-i])

                grad_b[-i] = delta
                if isinstance(delta, float):
                    d = delta
                else:
                    d = delta[:, np.newaxis]

                grad_w[-i] = acts[-i-1] * d
            if self.debug:
                for i, (w,b) in enumerate(zip(grad_w, grad_b)):
                    print('layer {0} to {1}'.format(i, i+1))
                    print('##################################\n'
                          '#############Weights#############')
                    print(w)
                    print('##################################\n'
                          '#############Biases###############')
                    print(b)

            grads.append([grad_w, grad_b])
        return grads

    def fit(self, train_data, valid_data, epoch, mini_batch, output=True):
        total_entries = len(train_data)
        for i_1 in range(epoch):
            print('Epoch No-{0}'.format(i_1))
            self.rand_g.shuffle(train_data)
            self.acc_score_train.append(self.accuracy_class(train_data))
            self.acc_score_valid.append(self.accuracy_class(valid_data))
            self.loss_score_train.append(self.cost(train_data))
            self.loss_score_valid.append(self.cost(valid_data))
            if output:
                print('Accuracy scores:'
                      '\nTraining-set = {0}%\nValidation-set = {1}%\n\n'
                      'Loss scores:'
                      '\nTraining-set = {2}\nValidation-set = {3}\n'.format(self.acc_score_train[-1]*100,
                                                                             self.acc_score_valid[-1]*100,
                                                                             self.loss_score_train[-1]/len(train_data),
                                                                             self.loss_score_valid[-1]/len(valid_data)))
                for j in range(0, len(train_data), mini_batch):
                    grads = self.compute_gradient(train_data[j:j+mini_batch])
                    self.weights, self.biases = self.gd_method.train(self.weights, self.biases,
                                                                     grads, mini_batch, total_entries)
        return self.acc_score_train, self.acc_score_valid, \
               self.loss_score_train, self.loss_score_valid


    # sum of the scores of the dataset + regularisation of weights
    # the final cost function to minimise is J(X) = C(X) + R(X)
    # where C(X) is the cost function and R(X) the regularisation method
    def cost(self, dataset):
        cost = 0
        for set in dataset:
            pred =self.predict(set[0])
            cost += self.e_func['id'](pred, set[1])
        reg = self.gd_method.reg.cost(self.weights, self.biases)
        return cost + len(dataset)*reg

    def accuracy_class(self, dataset):
        accuracy = 0
        for set in dataset:
            pred = self.predict(set[0])
            arg_pred = np.argmax(pred)
            arg_label = np.argmax(set[1])
            if arg_pred == arg_label:
                accuracy += 1
        return accuracy/len(dataset)

    def save(self, filename):
        np.save('./networks/nn_{filename}_weights.npy'.format(filename=filename), self.weights)
        np.save('./networks/nn_{filename}_biases.npy'.format(filename=filename), self.biases)

    def load(self, filename):
        self.weights = np.load('./networks/nn_{filename}_weights.npy'.format(filename=filename))
        self.biases = np.load('./networks/nn_{filename}_biases.npy'.format(filename=filename))

    def get_weights(self):
        return self.weights, self.biases

    def set_weights(self, weights, biases):
        self.weights = weights
        self.biases = biases

    def print_result(self, epoch, valid_acc, train_acc, valid_cost, train_cost):
        fig = plt.figure(1)
        ax = fig.add_subplot(211)
        ax.plot(range(epoch), valid_acc)
        ax.plot(range(epoch), train_acc)
        ax.set_xlim([0, epoch])
        ax.grid(True)
        ax.set_title('Classification accuracy\n{nb} classes'.format(nb=self.layers[-1]))
        ax.set_ylabel('Accuracy')
        ax.set_xlabel('epoch')
        ax.legend(['Validation', 'training'])

        ax = fig.add_subplot(212)
        ax.plot(range(epoch), valid_cost)
        ax.plot(range(epoch), train_cost)
        ax.set_xlim([0, epoch])
        ax.grid(True)
        ax.set_title('Error score: {function}'.format(function=self.e_func['name']))
        ax.set_ylabel('Cost')
        ax.set_xlabel('epoch')
        ax.legend(['Validation', 'training'])
        fig.suptitle(self.to_string())
        fig.subplots_adjust(top=0.863, bottom=0.066, left=0.055, right=0.99, hspace=0.279, wspace=0.0)

        fig.tight_layout()
        plt.show()


class MixedNN(NeuralNetwork):
    def fit(self, train_data, valid_data, epoch, mini_batch, output=True):
        for i_1 in range(epoch):
            print('Epoch No-{0}'.format(i_1))
            self.rand_g.shuffle(train_data)
            self.acc_score_train.append(self.accuracy_class(train_data))
            self.acc_score_valid.append(self.accuracy_class(valid_data))
            self.loss_score_train.append(self.cost(train_data))
            self.loss_score_valid.append(self.cost(valid_data))
            if output:
                print('Accuracy scores:'
                      '\nTraining-set = {0}%\nValidation-set = {1}%\n\n'
                      'Loss scores:'
                      '\nTraining-set = {2}\nValidation-set = {3}\n'.format(self.acc_score_train[-1] * 100,
                                                                            self.acc_score_valid[-1] * 100,
                                                                            self.loss_score_train[-1] / len(train_data),
                                                                            self.loss_score_valid[-1] / len(valid_data))
                                                                            )
            for j_1 in range(0, len(train_data), mini_batch):
                grads = self.compute_gradient(train_data[j_1:j_1 + mini_batch])
                w_grad = []
                b_grad = []
                for g in grads:
                    w_grad += g[0]
                    b_grad += g[1]
                w_grad = np.array(w_grad) / mini_batch
                b_grad = np.array(b_grad) / mini_batch
                previous_weights = self.weights.copy()
                previous_biases = self.biases.copy()
                l0 = self.cost(train_data[j_1:j_1 + mini_batch])
                params = self.gd_method.save()
                self.weights, self.biases = self.gd_method.train(self.weights, self.biases, grads, mini_batch,
                                                                 len(train_data))

                l1 = self.cost(train_data[j_1:j_1 + mini_batch])

                # print('l0 = {0}'.format(l0))
                find_better = l1 < l0
                if not find_better:
                    self.weights = previous_weights.copy()
                    self.biases = previous_biases.copy()
                    self.gd_method.reset(params)
                    #print('FIRST ORDER')
                    l0 = self.cost(train_data[j_1:j_1 + mini_batch])
                    w_grad_norm = 0
                    b_grad_norm = 0
                    for w, b in zip(w_grad, b_grad):
                        w_grad_norm += np.linalg.norm(w)
                        b_grad_norm += np.linalg.norm(b)
                    w_update = [-l0 * w_g / w_grad_norm for w_g in w_grad]
                    b_update = [-l0 * b_g / b_grad_norm for b_g in b_grad]
                    ld = 1
                    while not find_better:
                        previous_weights = self.weights.copy()
                        previous_biases = self.biases.copy()

                        w_update = np.array(w_update) * ld
                        b_update = np.array(b_update) * ld
                        self.weights = [w + w_up for w, w_up in zip(self.weights, w_update)]
                        self.biases = [b + b_up for b, b_up in zip(self.biases, b_update)]
                        l1 = self.cost(train_data[j_1:j_1 + mini_batch])
                        # print('l1 = {0}'.format(l1))
                        if l1 < l0:
                            # print('FIND BETTER')
                            w_hess = [np.empty(w.shape) for w in w_update]
                            b_hess = [np.empty(b.shape) for b in b_update]
                            for i_2 in range(len(w_update)):
                                for j_2 in range(len(w_update[i_2])):
                                    for k_2 in range(len(w_update[i_2][j_2])):
                                        w_hess[i_2][j_2][k_2] = 0 if w_update[i_2][j_2][k_2] == 0 else \
                                            -1 * w_grad[i_2][j_2][k_2] / w_update[i_2][j_2][k_2]
                            for i_2 in range(len(b_update)):
                                for j_2 in range(len(b_update[i_2])):
                                    b_hess[i_2][j_2] = 0 if b_update[i_2][j_2] == 0 else \
                                        -1 * b_grad[i_2][j_2] / b_update[i_2][j_2]
                            find_better = True
                            self.gd_method.train(None, None, None, None, None,
                                                 update_info=[w_update, b_update, w_hess, b_hess, w_grad, b_grad])
                            # print('ld = {0}'.format(ld))
                        else:
                            # print('NOT FIND BETTER')
                            self.weights = previous_weights.copy()
                            self.biases = previous_biases.copy()
                            ld *= 0.5
                        if ld <= 1e-16:
                            break
                else:
                    pass
                    #print('SECOND ORDER')
        return self.acc_score_train, self.acc_score_valid, \
               self.loss_score_train, self.loss_score_valid
        #self.print_result(epoch, self.acc_score_valid, self.acc_score_train, self.loss_score_valid, self.loss_score_train)



class FirstOrderNN(NeuralNetwork):
    def fit(self, train_data, valid_data, epoch, mini_batch, output=True):
        debug = False
        for i_1 in range(epoch):
            print('Epoch No-{0}'.format(i_1))
            self.rand_g.shuffle(train_data)
            self.acc_score_train.append(self.accuracy_class(train_data))
            self.acc_score_valid.append(self.accuracy_class(valid_data))
            self.loss_score_train.append(self.cost(train_data))
            self.loss_score_valid.append(self.cost(valid_data))
            if output:
                print('Accuracy scores:'
                      '\nTraining-set = {0}%\nValidation-set = {1}%\n\n'
                      'Loss scores:'
                      '\nTraining-set = {2}\nValidation-set = {3}\n'.format(self.acc_score_train[-1] * 100,
                                                                            self.acc_score_valid[-1] * 100,
                                                                            self.loss_score_train[-1] / len(train_data),
                                                                            self.loss_score_valid[-1] / len(
                                                                                valid_data)))
            for j_1 in range(0, len(train_data), mini_batch):
                grads = self.compute_gradient(train_data[j_1:j_1 + mini_batch])
                w_grad = []
                b_grad = []
                for g in grads:
                    w_grad += g[0]
                    b_grad += g[1]
                w_grad = np.array(w_grad) / mini_batch

                b_grad = np.array(b_grad) / mini_batch
                l0 = self.cost(train_data[j_1:j_1 + mini_batch])
                w_grad_norm = 0
                b_grad_norm = 0
                for w, b in zip(w_grad, b_grad):
                    w_grad_norm += np.linalg.norm(w)
                    b_grad_norm += np.linalg.norm(b)
                w_update = [-l0 * w_g / w_grad_norm for w_g in w_grad]
                b_update = [-l0 * b_g / b_grad_norm for b_g in b_grad]
                find_better = False
                ld = 1
                while not find_better:
                    previous_weights = self.weights.copy()
                    previous_biases = self.biases.copy()
                    w_update = np.array(w_update) * ld
                    b_update = np.array(b_update) * ld
                    self.weights = [w + w_up for w, w_up in zip(self.weights, w_update)]
                    self.biases = [b + b_up for b, b_up in zip(self.biases, b_update)]
                    l1 = self.cost(train_data[j_1:j_1 + mini_batch])
                    if debug:
                        print('l1 = {0}'.format(l1))
                    if l1 < l0:
                        if debug:
                            print('FIND BETTER')
                        find_better = True
                    else:
                        if debug:
                            print('NOT FIND BETTER')
                        self.weights = previous_weights.copy()
                        self.biases = previous_biases.copy()
                        ld *= 0.5
                    if debug:
                        print('ld = {0}'.format(ld))
                    if ld <= 1e-16:
                        break

        self.print_result(epoch, self.acc_score_valid, self.acc_score_train, self.loss_score_valid,
                          self.loss_score_train)
        return grads


class SecondOrderNN(NeuralNetwork):
    def fit(self, train_data, valid_data, epoch, mini_batch, output=True):
        for i_1 in range(epoch):
            print('Epoch No-{0}'.format(i_1))
            self.rand_g.shuffle(train_data)
            self.acc_score_train.append(self.accuracy_class(train_data))
            self.acc_score_valid.append(self.accuracy_class(valid_data))
            self.loss_score_train.append(self.cost(train_data))
            self.loss_score_valid.append(self.cost(valid_data))
            if output:
                print('Accuracy scores:'
                      '\nTraining-set = {0}%\nValidation-set = {1}%\n\n'
                      'Loss scores:'
                      '\nTraining-set = {2}\nValidation-set = {3}\n'.format(self.acc_score_train[-1] * 100,
                                                                            self.acc_score_valid[-1] * 100,
                                                                            self.loss_score_train[-1] / len(train_data),
                                                                            self.loss_score_valid[-1] / len(
                                                                                valid_data)))
            for i in range(3):
                grads = self.compute_gradient(train_data[i*mini_batch:(i+1)*mini_batch])
                w_grad = []
                b_grad = []
                for g in grads:
                    w_grad += g[0]
                    b_grad += g[1]
                w_grad = np.array(w_grad) / mini_batch
                b_grad = np.array(b_grad) / mini_batch
                ld = 1
                l0 = self.cost(train_data[0:mini_batch])
                w_grad_norm = 0
                b_grad_norm = 0
                for w, b in zip(w_grad, b_grad):
                    w_grad_norm += np.linalg.norm(w)
                    b_grad_norm += np.linalg.norm(b)
                w_update = [-l0 * w_g / w_grad_norm for w_g in w_grad]
                b_update = [-l0 * b_g / b_grad_norm for b_g in b_grad]
                find_better = False
                while not find_better:
                    previous_weights = self.weights.copy()
                    previous_biases = self.biases.copy()
                    w_update = np.array(w_update) * ld
                    b_update = np.array(b_update) * ld
                    self.weights = [w + w_up for w, w_up in zip(self.weights, w_update)]
                    self.biases = [b + b_up for b, b_up in zip(self.biases, b_update)]
                    l1 = self.cost(train_data[0:mini_batch])
                    # print('l1 = {0}'.format(l1))
                    if ld < 1e-16 or l1 < l0:
                        # print('FIND BETTER')
                        w_hess = [np.empty(w.shape) for w in w_update]
                        b_hess = [np.empty(b.shape) for b in b_update]
                        for i_2 in range(len(w_update)):
                            for j_2 in range(len(w_update[i_2])):
                                for k_2 in range(len(w_update[i_2][j_2])):
                                    w_hess[i_2][j_2][k_2] = 0 if w_update[i_2][j_2][k_2] == 0 else \
                                        -1 * w_grad[i_2][j_2][k_2] / w_update[i_2][j_2][k_2]
                        for i_2 in range(len(b_update)):
                            for j_2 in range(len(b_update[i_2])):
                                b_hess[i_2][j_2] = 0 if b_update[i_2][j_2] == 0 else \
                                    -1 * b_grad[i_2][j_2] / b_update[i_2][j_2]
                                self.gd_method.train(None, None, None, None, None,
                                                     update_info=[w_update, b_update, w_hess, b_hess, w_grad, b_grad])
                        find_better = True
                        # print('ld = {0}'.format(ld))
                    else:
                        # print('NOT FIND BETTER')
                        self.weights = previous_weights.copy()
                        self.biases = previous_biases.copy()
                        ld *= 0.5

            for j_1 in range(0, len(train_data), mini_batch):
                grads = self.compute_gradient(train_data[j_1:j_1 + mini_batch])
                self.weights, self.biases = self.gd_method.train(self.weights, self.biases, grads, mini_batch,
                                                                 len(train_data))
