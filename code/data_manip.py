import pandas as pd

df1 = pd.read_csv('/user/rferrand/home/PycharmProjects/vanilla_nn/code/csv/hp_relu.csv', index_col=0)
# df2 = pd.read_csv('/home/rferrand/PycharmProjects/vanilla_nn/code/csv/hp_test.csv', index_col=0)
df1['mean'] = df1[['acc_0','acc_1','acc_2','acc_3','acc_4','acc_5','acc_6','acc_7','acc_8', 'acc_9']].mean(axis=1)
df1['var'] = df1[['acc_0','acc_1','acc_2','acc_3','acc_4','acc_5','acc_6','acc_7','acc_8', 'acc_9']].var(axis=1)
df1.drop(['acc_0','acc_1','acc_2','acc_3','acc_4','acc_5','acc_6','acc_7','acc_8','acc_9','loss_0',
                'loss_1','loss_2','loss_3','loss_4','loss_5','loss_6','loss_7','loss_8','loss_9'], 1, inplace=True)
frame = [df1]
df = pd.concat(frame, ignore_index=True)
# print(df)
# df.to_csv('/user/rferrand/home/PycharmProjects/vanilla_nn/code/csv/parsed_sigmoid.csv')
df.to_csv('/user/rferrand/home/PycharmProjects/vanilla_nn/code/csv/parsed_relu.csv')