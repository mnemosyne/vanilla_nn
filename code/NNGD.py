import numpy as np
import sys


class KALMAN:
    def __init__(self, layers, reg):
        self.w_g_V = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.w_h_V = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.b_g_V = [np.zeros(y, dtype=float) for y in layers[1:]]
        self.b_h_V = [np.zeros(y, dtype=float) for y in layers[1:]]

        self.w_g_m = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.w_h_m = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.b_g_m = [np.zeros(y, dtype=float) for y in layers[1:]]
        self.b_h_m = [np.zeros(y, dtype=float) for y in layers[1:]]

        self.w_V_g = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.w_V_h = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.b_V_g = [np.zeros(y, dtype=float) for y in layers[1:]]
        self.b_V_h = [np.zeros(y, dtype=float) for y in layers[1:]]

        self.d_w = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.d_b = [np.zeros(y, dtype=float) for y in layers[1:]]
        self.w_g1 = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.b_g1 = [np.zeros(y, dtype=float) for y in layers[1:]]
        self.w_h1 = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.b_h1 = [np.zeros(y, dtype=float) for y in layers[1:]]
        self.reg = reg
        self.count = 0

    def save(self):
        return (self.w_g_V, self.w_h_V, self.b_g_V,
                self.b_h_V, self.w_g_m, self.w_h_m,
                self.b_g_m, self.b_h_m, self.w_V_g,
                self.w_V_h, self.b_V_g, self.b_V_h,
                self.d_w, self.d_b, self.w_g1, self.b_g1,
                self.w_h1, self.b_h1)

    def reset(self, params):
        (self.w_g_V, self.w_h_V, self.b_g_V,
         self.b_h_V, self.w_g_m, self.w_h_m,
         self.b_g_m, self.b_h_m, self.w_V_g,
         self.w_V_h, self.b_V_g, self.b_V_h,
         self.d_w, self.d_b, self.w_g1, self.b_g1,
         self.w_h1, self.b_h1) = params

    def update(self, w_update, b_update, w_hess, b_hess, w_grad, b_grad, layers):
        self.d_w = w_update
        self.d_b = b_update
        self.w_h1 = w_hess
        self.b_h1 = b_hess
        self.w_g1 = w_grad
        self.b_g1 = b_grad

    def to_string(self):
        return 'Kalman reg={0} '.format(self.reg.to_string())
    # update_info is None if update=False or an array like [w_update, b_update, w_hess, b_hess, w_grad, b_grad]
    def train(self, weights, biases, general_grads, mini_batch, nb_entries, update_info=None):

        if update_info is None:
            w_grad = []
            b_grad = []
            w_g = []
            b_g = []
            for g in general_grads:
                w_g.append(g[0])
                b_g.append(g[1])
            for g in general_grads:
                w_grad += g[0]
                b_grad += g[1]
            w_grad = np.array(w_grad) / mini_batch
            b_grad = np.array(b_grad) / mini_batch
            self.count += 1
            t = self.count
            w_g_V_t = self.w_g_V.copy()
            b_g_V_t = self.b_g_V.copy()
            w_h_V_t = self.w_h_V.copy()
            b_h_V_t = self.b_h_V.copy()
            self.w_g_V = [w_g_V + d_w ** 2 * w_h_V for w_g_V, d_w, w_h_V
                          in zip(self.w_g_V, self.d_w, self.w_h_V)]
            self.b_g_V = [b_g_V + d_b ** 2 * b_h_V for b_g_V, d_b, b_h_V
                          in zip(self.b_g_V, self.d_b, self.b_h_V)]
            self.w_h_V = [w_h_V + w_V_h for w_h_V, w_V_h in zip(self.w_h_V, self.w_V_h)]
            self.b_h_V = [b_h_V + b_V_h for b_h_V, b_V_h in zip(self.b_h_V, self.b_V_h)]

            self.w_g_m = [w_g_m + w_h_m * d_w for w_g_m, w_h_m, d_w in zip(self.w_g_m, self.w_h_m, self.d_w)]
            self.b_g_m = [b_g_m + b_h_m * d_b for b_g_m, b_h_m, d_b in zip(self.b_g_m, self.b_h_m, self.d_b)]
            for i, (vars, grads) in enumerate(zip(self.w_V_g, w_grad)):
                for j, (var, grad) in enumerate(zip(vars, grads)):
                    for k in range(len(var)):
                        self.w_V_g[i][j][k] = var[k] if var[k] > 0 else grad[k] ** 2 + 1e-15

            for i, (vars, grads) in enumerate(zip(self.b_V_g, b_grad)):
                for j, (var, grad) in enumerate(zip(vars, grads)):
                        self.b_V_g[i][j] = var if var > 0 else grad ** 2 + 1e-15

            for i, var in enumerate(self.w_g_V):
                for j, v in enumerate(var):
                    for k in range(len(v)):
                        w_g_V_t[i][j][k] = 1/v[k] if v[k] > 0 and self.count > 1 else 0
            for i, var in enumerate(self.b_g_V):
                for j, v in enumerate(var):
                        b_g_V_t[i][j] = 1/v if v > 0 and self.count > 1 else 0

            self.w_g_V = [1 / (1 / w_V_g + w_g_V_t) for w_V_g, w_g_V_t in zip(self.w_V_g, w_g_V_t)]
            self.b_g_V = [1 / (1 / b_V_g + b_g_V_t) for b_V_g, b_g_V_t in zip(self.b_V_g, b_g_V_t)]

            for i, var in enumerate(self.w_h_V):
                for j, v in enumerate(var):
                    for k in range(len(v)):
                        w_h_V_t[i][j][k] = 1 / v[k] if v[k] > 0 and self.count > 1 else 0
            for i, var in enumerate(self.b_h_V):
                for j, v in enumerate(var):
                        b_h_V_t[i][j] = 1 / v if v > 0 and self.count > 1 else 0

            w_h_V_t = [0.5 * w_d ** 2 / w_V_g + w_h_V_t for w_d, w_V_g, w_h_V_t
                       in zip(self.d_w, self.w_V_g, w_h_V_t)]
            b_h_V_t = [0.5 * b_d ** 2 / b_V_g + b_h_V_t for b_d, b_V_g, b_h_V_t
                       in zip(self.d_b, self.b_V_g, b_h_V_t)]

            for i, var in enumerate(w_h_V_t):
                for j, v in enumerate(var):
                    for k in range(len(v)):
                        self.w_h_V[i][j][k] = 1 / v[k] if v[k] > 0 and self.count > 1 else 0
            for i, var in enumerate(b_h_V_t):
                for j, v in enumerate(var):
                        self.b_h_V[i][j] = 1 / v if v > 0 and self.count > 1 else 0

            self.w_g_m = [w_g_V * (w_g / w_V_g + w_g_V_t * w_g_m)
                          for w_g_V, w_g, w_V_g, w_g_V_t, w_g_m in zip(self.w_g_V, w_grad, self.w_V_g, w_g_V_t, self.w_g_m)]
            self.b_g_m = [b_g_V * (b_g / b_V_g + b_g_V_t * b_g_m)
                          for b_g_V, b_g, b_V_g, b_g_V_t, b_g_m in zip(self.b_g_V, b_grad, self.b_V_g, b_g_V_t, self.b_g_m)]

            self.w_h_m = [w_h_V * ((0.5*(w_g - w_g1) * d_w) / w_V_g + w_h_V_t*w_h_m)
                          for w_h_V, w_g, w_g1, d_w, w_V_g, w_h_V_t, w_h_m
                          in zip(self.w_h_V, w_grad, self.w_g1, self.d_w, self.w_V_g, w_h_V_t, self.w_h_m)]
            self.b_h_m = [b_h_V * ((0.5 * (b_g - b_g1) * d_b) / b_V_g + b_h_V_t * b_h_m)
                          for b_h_V, b_g, b_g1, d_b, b_V_g, b_h_V_t, b_h_m
                          in zip(self.b_h_V, b_grad, self.b_g1, self.d_b, self.b_V_g, b_h_V_t, self.b_h_m)]

            self.w_V_g = [w_V_g + ((w_g - w_g_m)**2 - w_V_g)/t
                          for w_V_g, w_g, w_g_m in zip(self.w_V_g, w_grad, self.w_g_m)]
            self.b_V_g = [b_V_g + ((b_g - b_g_m)**2 - b_V_g)/t
                          for b_V_g, b_g, b_g_m in zip(self.b_V_g, b_grad, self.b_g_m)]
            self.w_V_h = [w_V_h + ((w_h_m - w_h1)**2 - w_V_h)/t
                          for w_V_h, w_h_m, w_h1 in zip(self.w_V_h, self.w_h_m , self.w_h1)]
            self.b_V_h = [b_V_h + ((b_h_m - b_h1)**2 - b_V_h)/t
                          for b_V_h, b_h_m, b_h1 in zip(self.b_V_h, self.b_h_m, self.b_h1)]

            for i, (w_g_m, w_h_m) in enumerate(zip(self.w_g_m, self.w_h_m)):
                for j, (w_g, w_h) in enumerate(zip(w_g_m, w_h_m)):
                    for k in range(len(w_g)):
                        self.d_w[i][j][k] = 0 if w_h[k] == 0 else -w_g[k]/w_h[k]
            for i, (b_g_m, b_h_m) in enumerate(zip(self.b_g_m, self.b_h_m)):
                for j, (b_g, b_h) in enumerate(zip(b_g_m, b_h_m)):
                        self.d_b[i][j] = 0 if b_h == 0 else -b_g/b_h
            _weights = [w + d_w for w, d_w in zip(weights, self.d_w)]
            _biases = [b + d_b for b, d_b in zip(biases, self.d_b)]

            self.w_h1 = self.w_h_m.copy()
            self.b_h1 = self.b_h_m.copy()
            self.w_g1 = w_grad.copy()
            self.b_g1 = b_grad.copy()
        else:
            self.w_g1 = self.w_g_m
            self.w_g_m = update_info[4]
            self.b_g1 = self.b_g_m
            self.b_g_m = update_info[5]
            self.w_h1 = self.w_h_m
            self.w_h_m = update_info[2]
            self.b_h1 = self.b_h_m
            self.b_h_m = update_info[3]
            self.d_w = update_info[0]
            self.d_b = update_info[1]
            _weights = None
            _biases = None

        return _weights, _biases


class SGD:
    def __init__(self, lr, reg):
        self.lr = lr
        self.reg = reg

    def to_string(self):
        return 'SGD(lr={0}) ' \
               'reg={1} '.format(self.lr, self.reg.to_string())

    def train(self, weights, biases, grads, mini_batch, nb_entries):
        w_grad = []
        b_grad = []
        for g in grads:
            w_grad += g[0]
            b_grad += g[1]
        w_grad = np.array(w_grad) / mini_batch
        b_grad = np.array(b_grad) / mini_batch

        weights = [current_w - self.lr * w_g - self.lr * self.reg.w(current_w) / nb_entries for
                   current_w, w_g in zip(weights, w_grad)]

        biases = [current_b - self.lr * b_g - self.lr * self.reg.b(current_b) / nb_entries for
                  current_b, b_g in zip(biases, b_grad)]
        return weights, biases

class ADADELTA:
    def __init__(self, layers, decay_rate, epsi, reg):
        self.decay_rate = decay_rate
        self.first_train = True
        self.lr = 1
        self.epsi = epsi
        self.reg = reg
        self.w_ac_grad = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.w_ac_delta = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.b_ac_grad = [np.zeros(y, dtype=float) for y in layers[1:]]
        self.b_ac_delta = [np.zeros(y, dtype=float) for y in layers[1:]]
        print(self.decay_rate)
        print(self.epsi)

    def to_string(self):
        return 'Adadelta=(gamma={0}, epsi={1}) ' \
               'reg={2} '.format(self.decay_rate, self.epsi, self.reg.to_string())

    def train(self, weights, biases, grads, mini_batch, nb_entries):
        w_grad = []
        b_grad = []
        for g in grads:
            w_grad += g[0]
            b_grad += g[1]
        w_grad = np.array(w_grad) / mini_batch
        b_grad = np.array(b_grad) / mini_batch
        self.w_ac_grad = [self.decay_rate*w_ac_g + (1 - self.decay_rate)*(w_g**2) for w_ac_g, w_g
                       in zip(self.w_ac_grad, w_grad)]

        self.b_ac_grad = [self.decay_rate*b_ac_g + (1 - self.decay_rate)*(b_g**2) for b_ac_g, b_g
                       in zip(self.b_ac_grad, b_grad)]
        if self.first_train:
            w_update = [-1*self.lr * w_g for
                        w_ac_g, w_ac_d, w_g in zip(self.w_ac_grad, self.w_ac_delta, w_grad)]
            b_update = [-1 * self.lr * b_g for
                        b_ac_g, b_ac_d, b_g in zip(self.b_ac_grad, self.b_ac_delta, b_grad)]
            self.first_train = False
        else:
            w_update = [(-1*(w_ac_d + self.epsi)**0.5 / (w_ac_g + self.epsi)**0.5)*w_g for
                        w_ac_g, w_ac_d, w_g in zip(self.w_ac_grad, self.w_ac_delta, w_grad)]

            b_update = [(-1 * (b_ac_d + self.epsi)**0.5 / (b_ac_g + self.epsi)**0.5) * b_g for
                        b_ac_g, b_ac_d, b_g in zip(self.b_ac_grad, self.b_ac_delta, b_grad)]

        self.w_ac_delta = [self.decay_rate*w_ac_d + (1 - self.decay_rate)*(w_up**2) for w_ac_d, w_up
            in zip(self.w_ac_delta, w_update)]

        self.b_ac_delta = [self.decay_rate*b_ac_d + (1 - self.decay_rate)*(b_up**2) for b_ac_d, b_up
            in zip(self.b_ac_delta, b_update)]

        weights = [w + d_w - self.reg.w(d_w)/nb_entries for w, d_w in zip(weights, w_update)]
        biases = [b + d_b - self.reg.b(d_b)/nb_entries for b, d_b in zip(biases, b_update)]

        return weights, biases

