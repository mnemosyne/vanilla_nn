from NNModelBuilder import *
from NNGD import *
from WeightRegression import *
from WeightInitializer import *
from NNFunctions import *
import numpy as np

layers = [1, 5, 1]
kf = KALMAN(layers, NoReg())
ga = GaussianAdversarial((layers, sigmoid, sigmoid, L2, normalized_variance, kf), 42, 24, 1000, 1000)
ga.run(100, 10)