import keras
from keras.callbacks import Callback
from tensorflow import set_random_seed

from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adadelta
import numpy as np
from keras import backend as K
from threading import Lock
import pandas as pd
lock = Lock()
"""
class StopWhenConstant(Callback):
    def __init__(self, monitor='val_acc'):
        super(Callback, self).__init__()
        self.monitor = monitor
        self.fiting = CurveFit()

    def on_epoch_end(self, epoch, logs={}):
        current = logs.get(self.monitor)
        self.fiting.add(current)
        if epoch >= 3 and self.fiting.getMode() == 'c':
            self.model.stop_training = True
"""
class ModelBuilder:
    def __init__(self, count, dataset, activation, output, loss, filename, seeds):
        if dataset == 'mnist':
            from keras.datasets import mnist
            (self.x_train, self.y_train), (self.x_test, self.y_test) = mnist.load_data()
            self.x_train = self.x_train.reshape(60000, 784)
            self.x_test = self.x_test.reshape(10000, 784)
            self.x_train = self.x_train.astype('float32')
            self.x_test = self.x_test.astype('float32')
            self.x_train /= 255
            self.x_test /= 255
            self.y_train = keras.utils.to_categorical(self.y_train, 10)
            self.y_test = keras.utils.to_categorical(self.y_test, 10)
            self.model = Sequential()
            self.model.add(Dense(512, activation=activation, input_shape=(784,)))
            self.model.add(Dense(512, activation=activation))
            self.model.add(Dense(10, activation=output))
        elif dataset == 'reuters':
            from keras.datasets import reuters
            from keras.preprocessing.text import Tokenizer
            max_words = 1000
            (self.x_train, self.y_train), (self.x_test, self.y_test) = reuters.load_data(num_words=max_words,
                                                                test_split=0.2)
            num_classes = np.max(self.y_train) + 1
            tokenizer = Tokenizer(num_words=max_words)
            self.x_train = tokenizer.sequences_to_matrix(self.x_train, mode='binary')
            self.x_test = tokenizer.sequences_to_matrix(self.x_test, mode='binary')
            self.y_train = keras.utils.to_categorical(self.y_train, num_classes)
            self.y_test = keras.utils.to_categorical(self.y_test, num_classes)
            self.model = Sequential()
            self.model.add(Dense(512, activation=activation, input_shape=(max_words,)))
            self.model.add(Dense(512, activation=activation))
            self.model.add(Dense(num_classes, activation=output))


        from ann_visualizer.visualize import ann_viz
        ann_viz(self.model, title="MNIST: network architecture")
        self.model.compile(loss=loss,
                           optimizer=Adadelta(),
                           metrics=['accuracy'])

        self.count = count
        self.filename = './csv/' + str(filename) +'.csv'
        if self.count == 0:
            acc = ['acc_{0}'.format(x) for x in seeds]
            loss = ['loss_{0}'.format(x) for x in seeds]
            acc = ','.join(acc)
            loss = ','.join(loss)
            csv_class = 'id,epsi,gamma,'+ acc+','+loss

            print('Warning: file descriptor opened')
            file = open(self.filename, 'a')
            file.write(csv_class+'\n')
            file.close()
            print('file descriptor closed')

    def run(self, seeds, batch_size, epochs, space, verbose=0):
        e = space['e']
        g = space['g']
        acc = ''
        loss = ''
        if verbose: print(space)
        for j, seed in enumerate(seeds):
            if verbose: print('seed No-{0}'.format(j+1))
            np.random.seed(seed)
            set_random_seed(seed)
            reset_weights(self.model, verbose)
            ada = Adadelta(rho=g, epsilon=e)
            self.model.optimizer = ada
            self.model.fit(self.x_train, self.y_train,
                      batch_size=batch_size,
                      epochs=epochs,
                      verbose=verbose,
                      validation_data=(self.x_test, self.y_test))#, callbacks=[StopWhenConstant()])
            score = self.model.evaluate(self.x_test, self.y_test, verbose=verbose)
            acc += ','+str(score[1])
            loss += ','+str(score[0])
        with lock:
            res = ','.join([str(self.count), str(e), str(g)])
            res += acc+loss+'\n'
            self.count += 1
            print('Warning: file descriptor opened')
            file = open(self.filename, 'a')
            file.write(res)
            file.close()
            print('file descriptor closed')
        return float('inf')


def reset_weights(model, verbose):
    session = K.get_session()
    for layer in model.layers:
        for v in layer.__dict__:
            v_arg = getattr(layer, v)
            if hasattr(v_arg, 'initializer'):
                initializer_method = getattr(v_arg, 'initializer')
                initializer_method.run(session=session)
                if verbose: print('reinitializing layer {}.{}'.format(layer.name, v))

