#! /usr/bin/env python3
from NNModelBuilder import *
from NNGD import *
from WeightRegression import *
from WeightInitializer import *
from NNFunctions import *
model = ModelBuilder('mnist_toy')
epoch = 100
mini_batch = 10
layers_and = [2, 5, 5, 2]
layers_mnist = [784, 512, 512, 10]
layers_mnist_toy = [64, 100, 10]
sgd = SGD(0.1, NoReg())
kf = KALMAN(layers_mnist_toy, NoReg())
#ada = ADADELTA(layers_mnist_toy, 0.95, 1e-5, NoReg())
#model.run(range(1), epoch, mini_batch, layers_mnist_toy, sigmoid,
#          softmax_f, cross_ent, normalized_variance, kf)
model.run(range(100, 1000), epoch, mini_batch, layers_mnist_toy, sigmoid,
          sigmoid, L2, normalized_variance, ['acc_train_kf', 'acc_valid_kf', 'loss_train_kf','loss_valid_kf'], kf)