from sklearn.preprocessing import LabelBinarizer
from NNGD import *
from NNFunctions import *
from NNBase import *
import pandas as pd
from sklearn.datasets import load_digits
import math
import keras

class ModelBuilder:
    def __init__(self, dataset):
        if dataset == 'mnist_toy':
            X, y = load_digits(return_X_y=True)
            cut = math.ceil(len(X) * 0.8)
            x_train = X[:cut]
            y_train = y[:cut]
            x_test = X[cut:]
            y_test = y[cut:]
            y_train = keras.utils.to_categorical(y_train, 10)
            y_test = keras.utils.to_categorical(y_test, 10)
        elif dataset == 'mnist':
            from keras.datasets import mnist
            (x_train, y_train), (x_test, y_test) = mnist.load_data()
            x_train = x_train.reshape(60000, 784)
            x_test = x_test.reshape(10000, 784)
            x_train = x_train.astype('float32')
            x_test = x_test.astype('float32')
            x_train /= 255
            x_test /= 255
            y_train = keras.utils.to_categorical(y_train, 10)
            y_test = keras.utils.to_categorical(y_test, 10)
        elif dataset =='and':
            train = pd.read_csv('and_train.csv')
            valid = pd.read_csv('and_valid.csv')
            x_train = train[['a', 'b']].values
            y_train = train['c'].values
            x_test = valid[['a', 'b']].values
            y_test = valid['c'].values
            y_train = keras.utils.to_categorical(y_train, 2)
            y_test = keras.utils.to_categorical(y_test, 2)
        self.train = [[img, label] for img, label in zip(x_train, y_train)]
        self.valid = [[img, label] for img, label in zip(x_test, y_test)]
        self.results = []

    def run(self, seeds, epoch, mini_batch, layers,
            act_func, out_func, error_func, w_init, filesname, gd_method):
        f = []
        for filename in filesname:
            temp = open(filename + ".csv", 'a+')
            if temp.tell() == 0:
                temp.write('epoch,' + str(filename) + '\n')
            f.append(temp)
        for i, seed in enumerate(seeds):
            print("start run N°{0}".format(i))
            gene = np.random.RandomState(seed)
            train = self.train
            valid = self.valid
            gene.shuffle(train)
            nn = MixedNN(layers, act_func=act_func,
                               out_func=out_func,
                               error_func=error_func,
                               w_init=w_init, gd_method=gd_method,
                               rand_gen=gene)
            self.results = nn.fit(train, valid, epoch, mini_batch)
            for res, file_desc in zip(self.results, f):
                for j, data in enumerate(res):
                    file_desc.write(str(j) + ',' + str(data) + '\n')
        for file_desc in f:
            file_desc.close()

    def output_csv(self, filename):
        mean_var_data = []
        for i, epsi in enumerate(self.epsis):
            for j, gamma in enumerate(self.gammas):
                mean = np.mean(self.results[i, j])
                var = np.var(self.results[i, j], ddof=1)
                mean_var_data.append([epsi, gamma, mean, var])
        df = pd.DataFrame(mean_var_data,
                          columns=['epsi', 'gamma', 'mean', 'var'])
        df.to_csv('./csv/mean_var_'+str(filename))

class AdadeltaModelBuilder(ModelBuilder):
    def run(self, seeds, epoch, mini_batch, layers,
            act_func, out_func, error_func, w_init, filesname, gd_method):
        f = []
        for filename in filesname:
            temp = open(filename + ".csv", 'a+')
            if temp.tell() == 0:
                temp.write('epsi,gamma,epoch,'+str(filename)+'\n')
            f.append(temp)
        for i, seed in enumerate(seeds):
            epsis = [1, 1e-1, 1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8,
                    1e-9, 1e-10, 1e-11, 1e-12, 1e-13, 1e-14, 1e-15, 1e-16]

            print("start run N°{0}".format(i))
            gene = np.random.RandomState(seed)
            epsi = gene.choice(epsis)
            gamma = round(gene.uniform(0, 1.001)/0.01)*0.01
            ada = ADADELTA(layers, gamma, epsi, NoReg())
            train = self.train
            valid = self.valid
            gene.shuffle(train)
            nn = NeuralNetwork(layers, act_func=act_func,
                               out_func=out_func,
                               error_func=error_func,
                               w_init=w_init, gd_method=ada,
                               rand_gen=gene)
            self.results = nn.fit(train, valid, epoch, mini_batch)
            for res, file_desc in zip(self.results, f):
                for j, data in enumerate(res):
                    file_desc.write(str(epsi)+','+str(gamma)+','+str(j)+','+str(data)+'\n')
        for file_desc in f:
            file_desc.close()


class GaussianAdversarial:
    def __init__(self, nn_params, seed1, seed2, nb_train, nb_valid):
        self.gene1 = np.random.RandomState(seed1)
        (layers, act_func,
        out_func, error_func, w_init,
        gd_method) = nn_params
        self.model_nn = NeuralNetwork(layers, act_func=act_func,
                               out_func=out_func,
                               error_func=error_func,
                               w_init=w_init, gd_method=gd_method,
                               rand_gen=self.gene1)
        self.train = []
        self.valid = []
        for _ in range(nb_train):

            x = self.gene1.randn()
            y = self.model_nn.predict(x)
            self.train.append([x, y])
        for _ in range(nb_valid):
            x = self.gene1.randn()
            y = self.model_nn.predict(x)
            self.valid.append([x, y])
        self.model_weights, self.model_biases = self.model_nn.get_weights()
        self.gene2 = np.random.RandomState(seed2)
        self.test_nn = NeuralNetwork(layers, act_func=act_func,
                               out_func=out_func,
                               error_func=error_func,
                               w_init=w_init, gd_method=gd_method,
                               rand_gen=self.gene2)
    def run(self, epoch, mini_batch):
        for _ in range(epoch):
            self.gene2.shuffle(self.train)
            for j in range(0, len(self.train), mini_batch):
                train = self.train[j:j+mini_batch]
                valid = self.valid
                self.test_nn.fit(train, valid, 1, mini_batch, True, True)
                grads_test = self.test_nn.compute_gradient(valid)
                grads_model = self.model_nn.compute_gradient(valid)
                w_g_test = 0
                w_b_test = 0
                w_g_model = 0
                w_b_model = 0

                for g1, g2 in zip(grads_test, grads_model):
                    w_g_test += (np.linalg.norm(g1[0]))
                    w_b_test += (np.linalg.norm(g1[1]))
                    w_g_model += (np.linalg.norm(g2[0]))
                    w_b_model += (np.linalg.norm(g2[1]))
                w_g_test /= len(self.valid)
                w_b_test /= len(self.valid)
                w_g_model /= len(self.valid)
                w_b_model /= len(self.valid)
                print('Test network gradients norm:\n'
                      'weights = {0}\n'
                      'biases = {1}'.format(w_g_test, w_b_test))
                print('Model network gradients norm:\n'
                      'weights = {0}\n'
                      'biases = {1}'.format(w_g_model, w_b_model))









