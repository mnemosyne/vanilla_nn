#! /usr/bin/env python3
from mnemonas import CurveFit
from mnemonas import Density
from math import exp

#from mnemonas import s_printf
#print(s_printf("Test\n"));

def assume(condition, test, message):
    print("Test = {0}".format(test))
    if(not condition):
        print()
        print("Fail: {0}".format(message))
    else:
        print("Pass")
def curvefit_tests():
    fit = CurveFit()
    [fit.add(1.2345) for _ in range(2)]
    assume(fit.getMode() == 'c' and abs(fit.getBias() - 1.2345) < 1e-12,
    "CurveFit_test constant", "wrong estimation : {0}\n".format(fit.asString()))
    fit.clear();
    [fit.add(1.2345 + Density.gaussian(0, 0.01)) for _ in range(10000)]
    assume(fit.getMode() == 'c' and abs(fit.getBias() - 1.2345) < 1e-3, "CurveFit_test constant + gaussian noise",
    "wrong estimation : {0}\n".format(fit.asString()))
    fit.clear()
    [fit.add(1.2345 + 0.12345 * (t - 2)) for t in range(3)]
    assume(fit.getMode() == 'a' and abs(fit.getGain() - 0.12345) < 1e-12 and abs(fit.getBias() - 1.2345) < 1e-12, "CurveFit_test affine",
    "wrong estimation : {0}\n".format(fit.asString()))
    fit.clear()
    [fit.add(1.2345 + 0.012345 * (t - 9999) + Density.gaussian(0, 0.001)) for t in range(10000)]
    assume(fit.getMode() == 'a' and abs(fit.getGain() - 0.012345) < 1e-2 and abs(fit.getBias() - 1.2345) < 1e-2, "CurveFit_test affine + gaussian noise",
    "wrong estimation : {0}\n".format(fit.asString()))
    fit.clear()
    [fit.add(2 * exp(-(t - 3) / 3.1416) + 1.2345) for t in range(4)]
    assume(fit.getMode() == 'e' and abs(fit.getDecay() - 3.1416) < 1e-4 and abs(fit.getBias() - 1.2345) < 1e-6, "CurveFit_test inverse exponential 1",
    "wrong estimation : {0}\n".format(fit.asString()))
    fit.clear()
    [fit.add(2 * exp((t - 3) / 3.1416) + 1.2345) for t in range(4)]
    assume(fit.getMode() == 'e' and abs(fit.getDecay() + 3.1416) < 1e-4 and abs(fit.getBias() - 1.2345) < 1e-6, "CurveFit_test exponential 1",
    "wrong estimation : {0}\n".format(fit.asString()))
    fit.clear()
    [fit.add(2 * exp(-(t - 99) / 31.416) + 1.2345) for t in range(100)]
    assume(fit.getMode() == 'e' and abs(fit.getDecay() - 31.416) < 1e-4 and abs(fit.getBias() - 1.2345) < 1e-4, "CurveFit_test inverse exponential 2",
    "wrong estimation : {0}\n".format(fit.asString()))
    fit.clear()
    [fit.add(-2 * exp(-(t - 99) / 31.416) + 1.2345) for t in range(100)]
    assume(fit.getMode() == 'e' and abs(fit.getDecay() - 31.416) < 1e-4 and abs(fit.getBias() - 1.2345) < 1e-4, "CurveFit_test inverse exponential 3",
    "wrong estimation : {0}\n".format(fit.asString()))
if __name__ == '__main__':
    curvefit_tests()
